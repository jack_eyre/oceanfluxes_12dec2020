"""
Plot vertical sections (latitude-pressure) of zonal mean differences 
between algorithms for atmo runs, for replication of E3SM-diags plots:

Call as:
    $ python AtmoDiffComparison_ZonalMeanSection_E3SMdiags.py var season interp
with command line arguments:
    var -- the variable: U
    season -- the averaging season: ANN, DJF, JJA 
    interp -- the interpolation method: bilin
All arguments are required and the script fails without them.

Layout of plot panels:
    -----------------------
    |  1. atmo: UA-ctl    |
    -----------------------
    |  2. atmo: COARE-ctl |
    -----------------------
    |  3. atmo: COARE-UA  |
    -----------------------

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 4:
        sys.exit("Incorrect number of command line arguments (3 required)")
    else:
        var_names = parse_var(argv[1])
        season = check_season(argv[2])
        interp = check_interp(argv[3])
    #
    # Specify height, lat and lon range.
    lat_range = np.array([-90.0,90.0])
    lon_range = np.array([0.0,360.0])
    p_range = np.array([1000.0,100.0])
    #
    # Load data.
    print('-----\nReading data...\n-----')
    plot_data,plot_names = load_data(var_names, season, interp, lon_range)
    #
    # Plot.
    print('-----\nPlotting... \n-----')
    plot_sections(plot_data, plot_names,
                  var_names, season, interp,
                  lat_range, lon_range, p_range)
    #
    return()


################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'U':
        var_name_dict['atmo'] = 'U'
    else:
        sys.exit('Variable name not valid - must be one of (U).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'JJA']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, JJA).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_data(v,s,i,lon_range):
    """Loads data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    #
    # Define file names and data types:
    if v['atmo'] in ['U']:
        ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/U_plev/'    
        UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/U_plev/'      
        COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/U_plev/'
    #
    filenames = [glob.glob(ctl_a_dir +
                           '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.nc'),
                 glob.glob(UA_a_dir +
                           '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.nc'),
                 glob.glob(COARE_a_dir +
                           '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.nc')]
    #
    # Define plot titles.
    clim_names = ['control',
                  'UA',
                  'COARE']
    #
    # Read data.
    clim_dict = {}
    for i_fn,fn in enumerate(filenames):
        ds = xr.open_dataset(fn[0], decode_times=False)
        if v['atmo'] in ['U']:
            clim_dict[clim_names[i_fn]] = ds[v['atmo']][0,:,:,:].\
                                          sel(lon=slice(lon_range[0],
                                                        lon_range[1])).\
                                          mean(dim='lon').\
                                          assign_coords(plev=ds.coords['plev']/\
                                                        100.0)
    #
    # Define plot titles.
    plot_names = ['atmosphere: UA - control',
                  'atmosphere: COARE - control',
                  'atmosphere: COARE - UA']
    #
    # Calculate differences.
    data_dict = {}
    data_dict[plot_names[0]] = clim_dict['UA'] - \
                               clim_dict['control']
    data_dict[plot_names[1]] = clim_dict['COARE'] - \
                               clim_dict['control']
    data_dict[plot_names[2]] = clim_dict['COARE'] - \
                               clim_dict['UA']
    #
    # Add control values for plotting.
    data_dict['atmosphere: control'] = clim_dict['control']
    #
    # Return data.
    return(data_dict, plot_names)


################################################################################
# Functions to plot maps.
################################################################################

def plot_sections(data_dict, name_list, var_name_dict, ssn, interp,
                  lat_range, lon_range, p_range):
    #
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['atmo'], ssn)
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up panel and axes.
    fig, axs = plt.subplots(3, 1,
                            figsize=(7, 7),
                            sharey=True, sharex=True)
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axs):
        #
        # Plot the data.
        p = ax.pcolormesh(data_dict[name_list[i]].lat,
                          data_dict[name_list[i]].plev,
                          data_dict[name_list[i]].data,
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          cmap=cmap_b, norm=norm_b)
        cn = ax.contour(data_dict['atmosphere: control'].lat,
                        data_dict['atmosphere: control'].plev,
                        data_dict['atmosphere: control'].data,
                        vmin=None,vmax=None,
                        levels=(-40.0,-30.0,-20.0,-10.0,-5.0,
                                5.0,10.0,20.0,30.0,40.0),
                        linewidths=[0.5],
                        colors=['grey'])
        #
        # Axis details.
        ax.invert_yaxis()
        ax.set_title(name_list[i],loc='left')
        ax.xaxis.set_tick_params(which='both',bottom=True,top=True)
        ax.yaxis.set_tick_params(which='both',left=True,right=True)
        ax.set_ylabel('pressure / ' + r'$hPa$')
    #
    # Add over all details.
    fig.suptitle(plot_details['main_title'])
    axs[-1].set_xlabel('latitude / ' + r'$^{\circ}N$')
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.025, 0.7])
    fig.colorbar(p, cax=cbar_ax, label=r'$m\ s^{-1}$', extend='both')
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + var_name_dict['atmo'] +
                '_' + ssn +
                '_' + interp +
                '.png',
                format='png')
    #
    return

    
def plot_info(v,s):
    """Give some details for plotting, based on variable name.
    Returns a dictionary with units, min and max for color scale.
    """
    # Format season stuff first.
    if s == 'ANN':
        s_string = 'annual mean '
    else:
        s_string = s + ' mean '
    #
    plot_info_dict = {}
    # General details.
    if v == 'U':                       
        plot_info_dict['min'] = -5.0      
        plot_info_dict['max'] = 5.0       
        plot_info_dict['color_step'] = 1.0
        plot_info_dict['units'] = r'$m~s^{-1}$'
        plot_info_dict['main_title'] = s_string + \
                                       'zonal wind speed'
    else:
        sys.exit('Variable name not valid - must be one of (U).')
    return(plot_info_dict)

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
