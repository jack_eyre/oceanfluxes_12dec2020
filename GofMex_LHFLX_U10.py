"""
Get latent heat flux and 10-m wind speed 3-hourly data; subset for Gulf of Mexico
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
###########################################

# Specify case name.
case_name = '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl'

# Specify data location details.
save_dir = '/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/' + \
           case_name + \
           '/atm/pp/'
data_dir = '/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/' + \
           case_name + \
           '/atm/hist/'
work_dir = '/global/homes/j/jeyre/code/oceanfluxes/'

# Specify filename formats.
filename_root = case_name + '.cam.h1.'
filename_tail = '-00000.nc'
filename_tail_new = '_GofMex_LHFLX_U10.nc'

# Useful flag for testing.
first_time = False


##### Subsetting details ######################################################

# Specify dates to include - will output one month per file.
time_min = datetime.datetime(2,1,1,0,0,0)
time_max = datetime.datetime(7,1,1,0,0,0)

# Spatial subset.
lat_range = np.array([22.5,27.5])
lon_range = np.array([265,275])


##### Loop over months ####### ###############################################
##### and get data for each.

def main():
    
    # Initialize monthly counter variable.
    ft = time_min 

    space_inds = space_subset(lat_range, lon_range)
    
    while ft < time_max:
        
        print(ft)
        
        # Get month and year.
        fm = ft.month
        fy = ft.year

        # Save out data for this month.
        save_LH_U(fy, fm, space_inds)

        # Increment for next month.
        ft = ft + datetime.timedelta(days=31)
        ft = ft - datetime.timedelta(days=(ft.day-1))


def save_LH_U(yy, mm, sp_inds):

    # Get arrays from first file of month.
    dd = 1
    dd_file_name = data_dir + filename_root + \
                   "{:04d}".format(yy) + '-' + \
                   "{:02d}".format(mm) + '-' + \
                   "{:02d}".format(dd) + filename_tail
    ds = xr.open_dataset(dd_file_name)
    LHF = ds['LHFLX'][:,sp_inds]
    SHF = ds['SHFLX'][:,sp_inds]
    U = ds['U10'][:,sp_inds]
    lat = ds['lat'][sp_inds]
    lon = ds['lon'][sp_inds]
    global first_time
    if first_time:
        print(LHF)
        print(SHF)
        print(U)
    
    # Get number of days in month.
    # Needs a trick to always give 28 in February due
    # to model calendar type.
    d_in_m = calendar.monthrange(1,mm)[1]

    # Loop over remaining days in month and concatenate arrays.
    dd = 2
    while dd <= d_in_m:
        dd_file_name = data_dir + filename_root + \
                   "{:04d}".format(yy) + '-' + \
                   "{:02d}".format(mm) + '-' + \
                   "{:02d}".format(dd) + filename_tail
        ds = xr.open_dataset(dd_file_name)
        LHF = xr.concat([LHF, ds['LHFLX'][:,sp_inds]],dim='time')
        SHF = xr.concat([SHF, ds['SHFLX'][:,sp_inds]],dim='time')
        U = xr.concat([U, ds['U10'][:,sp_inds]],dim='time')
        dd = dd + 1
        
    if first_time:
        print(LHF)
        print(SHF)
        print(U)
    
    # Postprocess arrays into dataset.
    ds_out = xr.Dataset({'LHFLX': (['time', 'ncol'], LHF),
                         'SHFLX': (['time', 'ncol'], SHF),
                         'U10': (['time', 'ncol'], U)},
                        coords={'time':(['time'],LHF.coords['time']),
                                'ncol':(['ncol'],np.arange(1,1+sum(sp_inds))),
                                'lat':(['ncol'],lat),
                                'lon':(['ncol'],lon)})
        
    if first_time:
        print(ds_out)
        print(ds_out.LHFLX)
        print(ds_out.SHFLX)
        first_time = False

    # Set up encoding.
    t_units = 'days since 0000-01-01 00:00:00'
    t_cal = 'noleap'
    fill_val = 9.96921e+36
    wr_enc = {'LHFLX':{'_FillValue':fill_val},
              'SHFLX':{'_FillValue':fill_val},
              'U10':{'_FillValue':fill_val},
              'time':{'units':t_units,'calendar':t_cal,
                      '_FillValue':fill_val},
              'ncol':{'_FillValue':fill_val},
              'lat':{'_FillValue':fill_val},
              'lon':{'_FillValue':fill_val}}
    

    # Add other metadata.
    ds_out.LHFLX.attrs['units'] = LHF.attrs['units']
    ds_out.LHFLX.attrs['long_name'] = LHF.attrs['long_name']
    ds_out.SHFLX.attrs['units'] = SHF.attrs['units']
    ds_out.SHFLX.attrs['long_name'] = SHF.attrs['long_name'] 
    ds_out.U10.attrs['units'] = U.attrs['units']
    ds_out.U10.attrs['long_name'] = U.attrs['long_name'] 
    ds_out.lat.attrs['units'] = lat.attrs['units']
    ds_out.lat.attrs['long_name'] = lat.attrs['long_name']
    ds_out.lon.attrs['units'] = lon.attrs['units']
    ds_out.lon.attrs['long_name'] = lon.attrs['long_name']
    ds_out.time.attrs['long_name'] = 'time'
    ds_out.ncol.attrs['long_name'] = 'space_index'
    ds_out.ncol.attrs['units'] = '-'
    ds_out.attrs['case_name'] = case_name
    ds_out.attrs['pp_comment'] = 'Files aggregated  by Jack Reeves Eyre (University of Arizona)'
    ds_out.attrs['pp_script'] = 'GofMex_LHFLX_U10.py'
    ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/oceanfluxes/src/master/'
    ds_out.attrs['pp_script_last_commit'] = get_git_revision_hash()
    ds_out.attrs['pp_conda_env'] = os.environ['CONDA_PREFIX']    
    ds_out.attrs['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # Construct file name.
    new_file_name = save_dir + filename_root + "{:04d}".format(yy) + \
                  '-' + "{:02d}".format(mm) + filename_tail_new


    # Save out file.
    ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])

def space_subset(lat_r, lon_r):

    # Read file.
    sp_file_name = data_dir + filename_root + \
                   "{:04d}".format(time_min.year) + '-' + \
                   "{:02d}".format(time_min.month) + '-' + \
                   "{:02d}".format(time_min.day) + filename_tail
    sp_file = xr.open_dataset(sp_file_name)
    lat_all = sp_file['lat']
    lon_all = sp_file['lon']

    # Get indices that match ranges.
    latlon_inds = (lat_all >= min(lat_range)) & \
                  (lat_all <= max(lat_range)) & \
                  (lon_all >= min(lon_range)) & \
                  (lon_all <= max(lon_range))
    print('Returning ' + str(sum(latlon_inds)) + ' grid points')
    return(latlon_inds)

def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash
    
# Now actually execute the script.
if __name__ == '__main__':
    main()
                   
        
