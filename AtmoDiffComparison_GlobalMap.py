"""
Plot global maps of differences between algorithms for atmo runs, for 
surface meteorology variables (precip, 2m temp. etc.):

Call as:
    $ python AtmoDiffComparison_GlobalMap.py var season interp
with command line arguments:
    var -- the variable: PREC, RESTOM, FLUT, FSNTOA, SWCF, LWCF, NETCF
    season -- the averaging season: ANN, DJF, MAM, JJA, SON 
    interp -- the interpolation method: conserve, bilin
All arguments are required and the script fails without them.

Layout of plot panels:
    -----------------------
    |  1. atmo: UA-ctl    |
    -----------------------
    |  2. atmo: COARE-ctl |
    -----------------------
    |  3. atmo: COARE-UA  |
    -----------------------

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
#
from SignificanceBenchmark_GlobalMap import *
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 4:
        sys.exit("Incorrect number of command line arguments (3 required)")
    else:
        var_names = parse_var(argv[1])
        season = check_season(argv[2])
        interp = check_interp(argv[3])
    #
    # Load data.
    print('-----\nReading data...\n-----')
    #
    # Get precip data if applicable.
    if var_names['atmo'] == 'PREC':
        plot_data,plot_names = load_precip_diff_data(var_names, season, interp)
        bias_data,bias_names = load_precip_bias_data(var_names, season, interp)
        #
        plot_data.update(bias_data)
        all_names = [None]*(len(bias_names) + len(plot_names))
        all_names[0::2] = plot_names
        all_names[1::2] = bias_names
        plot_names = all_names
    else:
        plot_data,plot_names = load_TOA_data(var_names, season, interp)
    #
    # Plot maps.
    print('-----\nPlotting... \n-----')
    if var_names['atmo'] == 'PREC':
        plot_precip_diffs(plot_data, plot_names,
                          var_names, season, interp)
    else:
        plot_toa_diffs(plot_data, plot_names,
                       var_names, season, interp)
    #
    return()

################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'PREC':
        var_name_dict['atmo'] = 'PREC'
        var_name_dict['atmo1'] = 'PRECC'
        var_name_dict['atmo2'] = 'PRECL'
        var_name_dict['CMIP'] = 'pr'
    elif v == 'RESTOM':
        var_name_dict['atmo'] = 'RESTOM'
        var_name_dict['atmo1'] = 'FSNT'
        var_name_dict['atmo2'] = 'FLNT'
        var_name_dict['CERES'] = 'toa_net_all_clim'
        # Note that sign convention is RESTOM = FSNT - FLNT.
    elif v == 'FLUT':
        var_name_dict['atmo'] = 'FLUT'
        var_name_dict['CERES'] = 'toa_lw_all_clim'
    elif v == 'FSNTOA':
        var_name_dict['atmo'] = 'FSNTOA'
        var_name_dict['CERES1'] = 'toa_sw_all_clim'
        var_name_dict['CERES2'] = 'solar_clim'
    elif v == 'SWCF':
        var_name_dict['atmo'] = 'SWCF'
        # Note the SWCF very close to (FSNT - FSNTC) and (FSNTOA - FSNTOAC).
        var_name_dict['CERES1'] = 'toa_sw_all_clim'
        var_name_dict['CERES2'] = 'toa_sw_clr_c_clim'
    elif v == 'LWCF':
        var_name_dict['atmo'] = 'LWCF'
        # Note that LWCF very close to (FLNTC - FLNT) and (FLUTC - FLUT).
        var_name_dict['CERES1'] = 'toa_lw_all_clim'
        var_name_dict['CERES2'] = 'toa_lw_clr_c_clim'
    elif v == 'NETCF':
        var_name_dict['atmo'] = 'NETCF'
        var_name_dict['atmo1'] = 'SWCF'
        var_name_dict['atmo2'] = 'LWCF'
        # Note that sign convention is NETCF = SWCF + LWCF
        var_name_dict['CERES1'] = 'toa_sw_all_clim'
        var_name_dict['CERES2'] = 'toa_sw_clr_c_clim'
        var_name_dict['CERES3'] = 'toa_lw_all_clim'
        var_name_dict['CERES4'] = 'toa_lw_clr_c_clim'
        # For CERES, SWCF = SWCLR - SWALL, LWCF = LWCLR - LWALL, and
        # NETCF = SWCF + LWCF
    else:
        sys.exit('Variable name not valid - must be one of (PREC, RESTOM, FLUT, FSNTOA, SWCF, LWCF, NETCF).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'MAM', 'JJA', 'SON']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['conserve', 'bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (conserve, bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_TOA_data(v,s,i):
    """ Loads top-of-atmosphere radiation data.
    Gives both differences between runs and biases vs. observations. 
    """
    #
    # Define file names and data types:
    ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/TOA/'
    UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/TOA/'
    COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/TOA/'
    filenames = [glob.glob(ctl_a_dir +
                           '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(UA_a_dir +
                           '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(COARE_a_dir +
                           '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc')]
    obs_dir = '~/Data/satellite/CERES/'
    obs_filename = 'CERES_EBAF-TOA_Ed4.1_1x1deg_Subset_CLIM01-CLIM12_20200623.nc'
    #
    # Add all data to array list - as final quantities.
    array_list = []
    for i_fn,fn in enumerate(filenames):
        ds = xr.open_dataset(fn[0], decode_times=False)
        #
        if v['atmo'] == 'RESTOM':
            array_list.append(ds[v['atmo1']][0,:,:] -
                              ds[v['atmo2']][0,:,:])
        elif v['atmo'] == 'FLUT':
            array_list.append(ds[v['atmo']][0,:,:])
        elif v['atmo'] == 'FSNTOA':
            array_list.append(ds[v['atmo']][0,:,:])
        elif v['atmo'] == 'SWCF':
            array_list.append(ds[v['atmo']][0,:,:])
        elif v['atmo'] == 'LWCF':
            array_list.append(ds[v['atmo']][0,:,:])
        elif v['atmo'] == 'NETCF':
            array_list.append(ds[v['atmo1']][0,:,:] +
                              ds[v['atmo2']][0,:,:])
        else:
            sys.exit('Variable name not valid - load_TOA_data takes one of (RESTOM, FLUT, FSNTOA, SWCF, LWCF, NETCF).')
    #
    # Add observational data.
    ds_obs = xr.open_dataset(obs_dir + obs_filename)
    if v['atmo'] == 'RESTOM':
        array_list.append(seasonal_mean_from_LTA(ds_obs[v['CERES']],s))
    elif v['atmo'] == 'FLUT':
        array_list.append(seasonal_mean_from_LTA(ds_obs[v['CERES']],s))
    elif v['atmo'] == 'FSNTOA':
        array_list.append(seasonal_mean_from_LTA(ds_obs[v['CERES2']],s) -
                          seasonal_mean_from_LTA(ds_obs[v['CERES1']],s))
    elif v['atmo'] == 'SWCF':
        array_list.append(seasonal_mean_from_LTA(ds_obs[v['CERES2']],s) -
                          seasonal_mean_from_LTA(ds_obs[v['CERES1']],s))
    elif v['atmo'] == 'LWCF':
        array_list.append(seasonal_mean_from_LTA(ds_obs[v['CERES2']],s) -
                          seasonal_mean_from_LTA(ds_obs[v['CERES1']],s))
    elif v['atmo'] == 'NETCF':
        array_list.append(seasonal_mean_from_LTA(ds_obs[v['CERES2']],s) -
                          seasonal_mean_from_LTA(ds_obs[v['CERES1']],s) +
                          seasonal_mean_from_LTA(ds_obs[v['CERES4']],s) -
                          seasonal_mean_from_LTA(ds_obs[v['CERES3']],s))
    else:
        sys.exit('Variable name not valid - load_TOA_data takes one of (RESTOM, FLUT, FSNTOA, SWCF, LWCF, NETCF).')
    #
    # Define plot titles.
    plot_names = ['atmosphere: UA - control',
                  'atmosphere: control - CERES-EBAF-4.1',
                  'atmosphere: COARE - control',
                  'atmosphere: UA - CERES-EBAF-4.1',
                  'atmosphere: COARE - UA',
                  'atmosphere: COARE - CERES-EBAF-4.1']
    plus_order = [1,0,2,1,2,2]
    minus_order = [0,3,0,3,1,3]
    #
    # Read data.
    data_dict = {}
    for i_diff,diff in enumerate(plot_names):
        data_dict[diff] = array_list[plus_order[i_diff]] - \
                          array_list[minus_order[i_diff]]
    #
    # Print some statistics.
    print('------------------------------' + v['atmo'] +
          '------------------------------')
    for b in plot_names:
        print('----------' + b + '----------')
        print('----------' + 'Mean bias' + '----------')
        print("{:+.4f}".format(global_mean(data_dict[b]).data))
        print('----------' + 'RMSD' + '----------')
        print("{:+.4f}".format(global_RMS(data_dict[b]).data))
    #
    # Print in LaTeX form.
    if 1:
        print('\\begin{table}[ht]')
        print('\\caption{INSERT CAPTION HERE}')
        print('\\centering')
        print('\\begin{tabular}{p{6cm} c c c c c c}')
        print(' &  & Bias &  &  & RMSD &  \\\\')
        print('\\hline')
        print(' & control & UA & COARE & control & UA & COARE \\\\')
        print('\\hline')
        print(v['atmo'] + '$\ (W~m^{-2})\ $' + ' & ' +
              "{:+.2f}".format(global_mean(data_dict[plot_names[1]]).data) +
              ' & ' +
              "{:+.2f}".format(global_mean(data_dict[plot_names[3]]).data) +
              ' & ' +
              "{:+.2f}".format(global_mean(data_dict[plot_names[5]]).data) +
              ' & ' +
              "{:+.2f}".format(global_RMS(data_dict[plot_names[1]]).data) +
              ' & ' +
              "{:+.2f}".format(global_RMS(data_dict[plot_names[3]]).data) +
              ' & ' +
              "{:+.2f}".format(global_RMS(data_dict[plot_names[5]]).data) +
              ' & ')
        print('\\end{tabular}')
        print('\\label{obs_bias_table}')
        print('\\end{table}')
    #
    # Return data.
    return(data_dict, plot_names)

def load_precip_diff_data(v,s,i):
    """Loads precip data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    #
    # Define file names and data types:
    ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/SurfaceMet/'
    UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/SurfaceMet/'
    COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/SurfaceMet/'
    filenames = [glob.glob(UA_a_dir +
                           '20190521.v1_UA.minus.20190521.v1_ctl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(COARE_a_dir +
                           '20190621.v1_COARE.minus.20190521.v1_ctl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(COARE_a_dir +
                           '20190621.v1_COARE.minus.20190521.v1_UA_' +
                           s + '_*_climo.' + i + '.nc')]
    #
    # Define plot titles based on above files.
    plot_names = ['atmosphere: UA - control',
                  'atmosphere: COARE - control',
                  'atmosphere: COARE - UA']
    #
    # Read data.
    data_dict = {}
    for i_fn,fn in enumerate(filenames):
        ds = xr.open_dataset(fn[0], decode_times=False)
        if v['atmo'] == 'PREC':
            data_dict[plot_names[i_fn]] = (ds[v['atmo1']][0,:,:] +
                                           ds[v['atmo2']][0,:,:])*\
                                           (1000.0*60.0*60.0*24.0)
            data_dict[plot_names[i_fn]].attrs['units'] = 'mm day-1'
        else:
            data_dict[plot_names[i_fn]] = ds[v['atmo']][0,:,:]
    #
    # Return data.
    return(data_dict, plot_names)
    

def load_precip_bias_data(v,s,i):
    """Loads precip bias (vs. observations) data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    #
    # Define file names and data types:
    ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/SurfaceMet/'
    UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/SurfaceMet/'
    COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/SurfaceMet/'
    filenames = [glob.glob(ctl_a_dir +
                           '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(UA_a_dir +
                           '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(COARE_a_dir +
                           '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc')]
    #
    # Load GPCP data.
    GPCP_filename = '/home/jackre/Data/satellite/GPCP/' + \
                    'GPCP.v2.3.precip.mon.ltm_180x360_GreenwichWest_' + \
                    i + '.nc'
    ds_GPCP = xr.open_dataset(GPCP_filename)
    # Take annual mean (weighted).
    pr_GPCP = seasonal_mean_from_LTA(ds_GPCP['precip'],s)
    #
    # Define plot titles based on above files.
    plot_names = ['atmosphere: control - GPCP',
                  'atmosphere: UA - GPCP',
                  'atmosphere: COARE - GPCP']
    #
    # Read data.
    data_dict = {}
    for i_fn,fn in enumerate(filenames):
        ds = xr.open_dataset(fn[0], decode_times=False)
        if v['atmo'] == 'PREC':
            data_dict[plot_names[i_fn]] = (ds[v['atmo1']][0,:,:] +
                                           ds[v['atmo2']][0,:,:])*\
                                           (1000.0*60.0*60.0*24.0) - \
                                           pr_GPCP
            data_dict[plot_names[i_fn]].attrs['units'] = 'mm day-1'
        else:
            sys.exit('load_precip_bias_data only works for variable PREC.')
    #
    # Print some statistics.
    for b in plot_names:
        print('----------' + b + '----------')
        print('----------' + 'Mean bias' + '----------')
        print("{:+.4f}".format(global_mean(data_dict[b]).data))
        print('----------' + 'RMSD' + '----------')
        print("{:+.4f}".format(global_RMS(data_dict[b]).data))
    #
    # Return data.
    return(data_dict, plot_names)

def seasonal_mean_from_LTA(LTA,ssn):
    """Calculates weighted seasonal (or annual) mean from monthly long 
    term average data (i.e, an array with size (12 x nlat x nlon)).
    """
    #
    # Define days per month.
    dpm = np.array([31,28,31,30,31,30,31,31,30,31,30,31])
    #
    # Get months for the season - ZERO INDEXED!.
    if ssn == 'ANN':
        s_m = np.arange(0,12,dtype=np.int)
    elif ssn == 'DJF':
        s_m = np.array([0,1,11],dtype=np.int)
    elif ssn == 'MAM':
        s_m = np.array([2,3,4],dtype=np.int)
    elif ssn == 'JJA':
        s_m = np.array([5,6,7],dtype=np.int)
    elif ssn == 'SON':
        s_m = np.array([8,9,10],dtype=np.int)
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    #
    # Calculate days in this season.
    dps = np.sum(dpm[s_m])
    #
    # Get first month and weight it.
    ann_LTA = LTA[s_m[0],:,:]*dpm[s_m[0]]
    #
    # Loop over other months.
    for i in range(1,len(s_m)):
        ann_LTA = ann_LTA + LTA[s_m[i],:,:]*dpm[s_m[i]]
    #
    # Divide by days in season.
    ann_LTA = ann_LTA/dps
    #
    return ann_LTA

################################################################################
# Functions to calculate stats.
################################################################################

# Calculate the global mean value of a global map.
# Assumes regular grid and applies cosine weighting.
def global_mean(diff_map):
    #
    # Create map of cosine(latitude) weights.
    cos_map = xr.zeros_like(diff_map)
    cos_map.data = np.broadcast_to(np.expand_dims(
        np.cos(diff_map.lat*np.pi/180.0),axis=1),
                                   diff_map.shape)
    #
    # Calculate mean.
    diff_mean = (diff_map*cos_map).sum()/(cos_map.sum())
    #
    return diff_mean

# Calculate the global RMS value of a global map.
# Assumes regular grid and applies cosine weighting.
def global_RMS(diff_map):
    #
    # Create map of cosine(latitude) weights.
    cos_map = xr.zeros_like(diff_map)
    cos_map.data = np.broadcast_to(np.expand_dims(
        np.cos(diff_map.lat*np.pi/180.0),axis=1),
                                   diff_map.shape)
    #
    # Calculate RMS.
    diff_RMS = np.sqrt((diff_map*diff_map*cos_map).sum()/(cos_map.sum()))
    #
    return diff_RMS

################################################################################
# Functions to plot maps.
################################################################################

def plot_precip_diffs(data_dict, name_list, var_name_dict, ssn, interp):
    """Plot precip maps.
    """
    if (var_name_dict['atmo'] != 'PREC'):
        sys.exit('plot_precip_diffs only works for PREC')
    #
    # Get variable for hatching.
    sig_var = load_E3SM_std(var_name_dict,ssn)
    sig_var = sig_var*(60.0*60.0*24.0)
    sig_var.attrs['units'] = 'mm day-1'
    #
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['atmo'], ssn)
    #
    # Define color map.
    cmap_b = plt.get_cmap('BrBG')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 2),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axgr):
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[name_list[i]].coords['lat'],
                                 data_dict[name_list[i]].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(name_list[i],loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm, data_dict[name_list[i]].data,
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        #
        # Add hatching for significant values.
        hatch_var = np.where(np.abs(data_dict[name_list[i]].data) >= sig_var,
                             1.0, 0.0)
        hatch = ax.contourf(lonm, latm, hatch_var,
                            levels=np.array([-0.5,0.5,1.5]),
                            colors='none',
                            hatches=[None, '...'])
        hatch = ax.contour(lonm, latm, hatch_var,
                           levels=np.array([-0.5,0.5,1.5]),
                           colors='black',
                           linewidths=0.3)
        #
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    #
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p)
    cb.set_label_text(plot_details['units'])
    #
    # Add over all details.
    fig.suptitle(plot_details['main_title'])
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + var_name_dict['atmo'] +
                '_' + ssn +
                '_' + interp +
                '.png',
                format='png')

    return()


def plot_toa_diffs(data_dict, name_list, var_name_dict, ssn, interp):
    """Plot precip maps.
    """
    if (var_name_dict['atmo'] not in ['RESTOM', 'FLUT', 'FSNTOA',
                                      'SWCF', 'LWCF', 'NETCF']):
        sys.exit('plot_toa_diffs only works for RESTOM, FLUT, FSNTOA, SWCF, LWCF, NETCF')
    #
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['atmo'], ssn)
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 2),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axgr):
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[name_list[i]].coords['lat'],
                                 data_dict[name_list[i]].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(name_list[i],loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm, data_dict[name_list[i]].data,
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        #
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    #
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p)
    cb.set_label_text(plot_details['units'])
    #
    # Add over all details.
    fig.suptitle(plot_details['main_title'])
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + var_name_dict['atmo'] +
                '_' + ssn +
                '_' + interp +
                '.png',
                format='png')

    return()

def plot_info(v,s):
    """Give some details for plotting, based on variable name.
    Returns a dictionary with units, min and max for color scale.
    """
    toa_long_names = {'RESTOM':'top of model net radiation',
                      'FLUT':'top of model upward longwave',
                      'FSNTOA':'top of atmosphere net shortwave',
                      'SWCF':'top of atmosphere shortwave cloud forcing',
                      'LWCF':'top of atmosphere longwave cloud forcing',
                      'NETCF':'top of atmosphere net cloud forcing'}
    plot_info_dict = {}
    # Season-specific bits.
    if s == 'ANN':
        s_string = 'annual mean '
        if v == 'PREC':                       
            plot_info_dict['min'] = -2.0      
            plot_info_dict['max'] = 2.0       
            plot_info_dict['color_step'] = 0.2
    else:
        s_string = s + ' mean '
        if v == 'PREC':        
            plot_info_dict['min'] = -5.0      
            plot_info_dict['max'] = 5.0       
            plot_info_dict['color_step'] = 1.0
    # General details.
    if v == 'PREC':
        plot_info_dict['units'] = r'$mm\ day^{-1}$'
        plot_info_dict['main_title'] = s_string + \
                                       'total precipitation'
    elif v in ['RESTOM', 'FLUT', 'FSNTOA',
               'SWCF', 'LWCF', 'NETCF']:
        plot_info_dict['units'] = r'$W\ m^{-2}$'
        plot_info_dict['main_title'] = s_string + \
                                       toa_long_names[v]
        plot_info_dict['min'] = -40.0      
        plot_info_dict['max'] = 40.0       
        plot_info_dict['color_step'] = 10.0
        
    else:
        sys.exit('Variable name not valid - must be one of (PREC).')
    return(plot_info_dict)

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
