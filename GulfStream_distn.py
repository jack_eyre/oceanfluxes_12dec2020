"""
Plot CDF of wind speed, and LHFLX stats conditional on wind speed.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import sys
import os
import subprocess
import matplotlib.pyplot as plt
###########################################

# Specify data locations.
case_names = ['20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl', '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl',
              '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl']
data_dirs = ['/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/pp/',
             '/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/pp/',
             '/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/pp/']
           
# Plot details.
plot_dir = '/global/homes/j/jeyre/plots/oceanfluxes/'
plot_name = plot_dir + 'GulfStream_distn_newColors.pdf'


###### Read in data.################################################

# Specify times to read.
times = [datetime.datetime(2,1,1,0,0,0),
         datetime.datetime(2,2,1,0,0,0),
         datetime.datetime(2,12,1,0,0,0),
         datetime.datetime(3,1,1,0,0,0),
         datetime.datetime(3,2,1,0,0,0),
         datetime.datetime(3,12,1,0,0,0),
         datetime.datetime(4,1,1,0,0,0),
         datetime.datetime(4,2,1,0,0,0),
         datetime.datetime(4,12,1,0,0,0),
         datetime.datetime(5,1,1,0,0,0),
         datetime.datetime(5,2,1,0,0,0),
         datetime.datetime(5,12,1,0,0,0),
         datetime.datetime(6,1,1,0,0,0),
         datetime.datetime(6,2,1,0,0,0),
         datetime.datetime(6,12,1,0,0,0)] 

# Initialize variable to hold answers.
ds = []

# Loop over datasets and add data to list.
for i in range(len(case_names)):
    
    # Get list of files to load.
    filelist = []
    for t in range(len(times)):
        filelist.append(data_dirs[i] + case_names[i] + '.cam.h1.' + \
                        "{:04d}".format(times[t].year) + '-' + \
                        "{:02d}".format(times[t].month) + \
                        '_GulfStream_LHFLX_SHFLX_U10.nc')

    # Read in files as a single dataset.
    ds.append(xr.open_mfdataset(filelist, combine='by_coords'))


###### Calculate quantiles.###########################################
#      of latent heat flux, conditional on wind speed.

# Define wind speed bins.
delta_u = 1
u10_bins = np.arange(2,31,delta_u)
n_bins = u10_bins.size + 1

# Define desired quantiles.
quantiles = [0.05, 0.5, 0.95]

# Initialize arrays to hold answers.
lh_quants = np.empty([len(case_names),n_bins,len(quantiles)],
                     dtype='float64')
lh_quants[:] = np.nan
u_counts = np.empty([len(case_names),n_bins],
                     dtype='float64')
u_counts[:] = np.nan
u_vals = np.empty([n_bins], dtype='float64')
u_labels = np.empty([n_bins], dtype=object)

# Loop over cases.
for i in range(len(case_names)):
    
    # Load the data.
    LH = ds[i].LHFLX.load().data
    U = ds[i].U10.load().data
    
    # Loop over wind speed bins.
    for j in range(n_bins):
        
        # Get mask for this wind speed bin.
        if (j == 0):
            U_mask = (U < u10_bins[j])
        elif (j == n_bins-1):
            U_mask = (U >= u10_bins[j-1])
        else:
            U_mask = (U >= u10_bins[j-1]) & (U < u10_bins[j])
        
        # Calculate the quantiles.
        if (np.sum(U_mask) >= LH.size/500.0):
            lh_quants[i,j,:] = np.quantile(LH[U_mask], quantiles)
        
        # Calculate the bin count.
        u_counts[i,j] = np.sum(U_mask)
        
        # Get label values etc. for plotting.
        if (i == 0):
            if (j == 0):
                u_vals[j] = u10_bins[j] - (delta_u*0.5)
                u_labels[j] = '< ' + str(u10_bins[j])
            elif (j == n_bins-1):
                u_vals[j] = u10_bins[j-1] + (delta_u*0.5)
                u_labels[j] = '> ' + str(u10_bins[j-1])
            else:
                u_vals[j] = (u10_bins[j-1] + u10_bins[j])*0.5
                u_labels[j] = str(u10_bins[j-1]) + '-' + str(u10_bins[j])
print(np.sum(u_counts,axis=1))


###### Plot figure.###################################################

# Convert counts to relative frequency and
# don't plot zero counts.
u_counts[0,:] = u_counts[0,:]/np.sum(u_counts[0,:])
u_counts[1,:] = u_counts[1,:]/np.sum(u_counts[1,:])
u_counts[2,:] = u_counts[2,:]/np.sum(u_counts[2,:])
u_counts[u_counts == 0] = np.nan

# Main figure definition.
plt.figure(1)
plt.suptitle('Gulf Stream/North Atlantic (60-70W, 35-42N)\nDJF years 2-6, 3 hourly')

# First sub-plot - binned LH flux quantiles.
ls_mid = 'solid'
ls_hilo = 'dotted'
col_UA = '#6c71c4'
col_ctl = '#268bd2'
col_COARE = '#dc322f'
plt.subplot(211)
plt.plot(u_vals, lh_quants[0,:,1], color=col_UA, linestyle=ls_mid, label='UA median')
plt.plot(u_vals, lh_quants[0,:,0], color=col_UA, linestyle=ls_hilo)#, label='UA 5th %ile')
plt.plot(u_vals, lh_quants[0,:,2], color=col_UA, linestyle=ls_hilo)#, label='UA 95th %ile')
plt.plot(u_vals, lh_quants[1,:,1], color=col_ctl, linestyle=ls_mid, label='CTL median')
plt.plot(u_vals, lh_quants[1,:,0], color=col_ctl, linestyle=ls_hilo)#, label='CTL 5th %ile')
plt.plot(u_vals, lh_quants[1,:,2], color=col_ctl, linestyle=ls_hilo)#, label='CTL 95th %ile')
plt.plot(u_vals, lh_quants[2,:,1], color=col_COARE, linestyle=ls_mid, label='COARE median')
plt.plot(u_vals, lh_quants[2,:,0], color=col_COARE, linestyle=ls_hilo)#, label='COARE 5th %ile')
plt.plot(u_vals, lh_quants[2,:,2], color=col_COARE, linestyle=ls_hilo)#, label='COARE 95th %ile')
#plt.xticks(u_vals, '')
plt.xlim(1,31)
plt.ylim(-200,1000)
plt.ylabel('LH flux / ' + r'$W m^{-2}$')
plt.legend(fontsize='small')

# Second sub-plot - wind speed counts. 
plt.subplot(212)
plt.plot(u_vals, u_counts[0,:], color=col_UA, label='UA')
plt.plot(u_vals, u_counts[1,:], color=col_ctl, label='control')
plt.plot(u_vals, u_counts[2,:], color=col_COARE, label='COARE')
#plt.xticks(u_vals, u_labels, rotation='vertical')
plt.xlim(1,31)
plt.ylim(0,0.12)
plt.ylabel('Frequency')
plt.xlabel('10m wind speed / ' + r'$m s^{-1}$')
plt.legend()


# Save figure as PDF.
plt.savefig(plot_name, format='pdf')
