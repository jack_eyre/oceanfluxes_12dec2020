#!/bin/bash
# This software is open source software available under the BSD-3 license.
#
# Copyright (c) 2019 Triad National Security, LLC. All rights reserved.
# Copyright (c) 2019 Lawrence Livermore National Security, LLC. All rights
# reserved.
# Copyright (c) 2019 UT-Battelle, LLC. All rights reserved.
#
# Additional copyright and license information can be found in the LICENSE file
# distributed with this code, or at
# https://raw.githubusercontent.com/MPAS-Dev/MPAS-Analysis/master/LICENSE

# Old setting for PBS scheduler (?).
# # comment out if using debug queue
# #PBS -q acme
# #PBS -A ACME
# #PBS -l nodes=1
# #PBS -l walltime=1:00:00
# #PBS -N mpas_analysis
# #PBS -o mpas_analysis.o$PBS_JOBID
# #PBS -e mpas_analysis.e$PBS_JOBID

## slurm job options
#SBATCH -A condo
#SBATCH -p acme-centos6
#SBATCH --nodes=1
#SBATCH --time=1:00:00
#SBATCH --job-name=mpas_analysis
#SBATCH --output=mpas_analysis.o%j
#SBATCH --error=mpas_analysis.e%j

cd $SLURM_SUBMIT_DIR

export OMP_NUM_THREADS=1

source /lcrc/soft/climate/e3sm-unified/load_latest_e3sm_unified.sh
export HDF5_USE_FILE_LOCKING=FALSE

# This used to be in here, not sure if its still needed.
#  # needed to prevent interference with acme-unified
#  unset LD_LIBRARY_PATH


# MPAS/ACME job to be analyzed, including paths to simulation data and
# observations. Change this name and path as needed
run_config_file="/home/jeyre/mpas_analysis/config.20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil"

if [ ! -f $run_config_file ]; then
    echo "File $run_config_file not found!"
    exit 1
fi

srun -N 1 -n 1 mpas_analysis $run_config_file
