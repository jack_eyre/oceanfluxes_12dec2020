"""
Plot global maps of differences between algorithms for atmo runs, for 
surface meteorology variables (precip, 2m temp. etc.):

Call as:
    $ python AtmoDiffComparison_GlobalMap.py var season interp
with command line arguments:
    var -- the variable: SSH
    season -- the averaging season: ANN, DJF, MAM, JJA, SON 
    interp -- the interpolation method: conserve, bilin
All arguments are required and the script fails without them.

Layout of plot panels:
    -----------------------
    |  1.  ocn: UA-ctl    |
    -----------------------
    |  2.  ocn: COARE-ctl |
    -----------------------
    |  3.  ocn: COARE-UA  |
    -----------------------

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
#
from SignificanceBenchmark_GlobalMap import *
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 4:
        sys.exit("Incorrect number of command line arguments (3 required)")
    else:
        var_names = parse_var(argv[1])
        season = check_season(argv[2])
        interp = check_interp(argv[3])
    #
    # Load data.
    print('\nReading data...\n')
    plot_data,plot_names = load_data(var_names, season, interp)
    #
    # Plot maps.
    print('\nPlotting... \n')
    plot_diffs(plot_data, plot_names, var_names, season, interp)
    #
    return()

################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'SSH':
        var_name_dict['short_name'] = 'SSH'
        var_name_dict['ocn'] = 'timeMonthly_avg_pressureAdjustedSSH'
    else:
        sys.exit('Variable name not valid - must be one of (SSH).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'MAM', 'JJA', 'SON']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['conserve', 'bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (conserve, bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_data(v,s,i):
    """Loads data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    #
    # Define file names and data types:
    # Years for climatology.
    clim_yr_str = '0010[0-9][0-9]_0010[0-9][0-9]'
    # Directories.
    ctl_o_dir = '/home/jackre/Data/E3SM/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/SSH/'
    UA_o_dir = '/home/jackre/Data/E3SM/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/SSH/'
    COARE_o_dir = '/home/jackre/Data/E3SM/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/SSH/'
    # List file names.
    filenames = [glob.glob(UA_o_dir +
                           '20191008.ocnSfcFlx_UA.minus.' +
                           '20191007.ocnSfcFlx_ctl_mpaso_' +
                           s + '_' + clim_yr_str + '_climo.nc'),
                 glob.glob(COARE_o_dir +
                           '20191008.ocnSfcFlx_COARE.minus.' +
                           '20191007.ocnSfcFlx_ctl_mpaso_' +
                           s + '_' + clim_yr_str + '_climo.nc'),
                 glob.glob(COARE_o_dir +
                           '20191008.ocnSfcFlx_COARE.minus.' +
                           '20191008.ocnSfcFlx_UA_mpaso_' +
                           s + '_' + clim_yr_str + '_climo.nc')]
    #
    # Define plot titles based on above files.
    plot_names = ['ocean: UA - control',
                  'ocean: COARE - control',
                  'ocean: COARE - UA']
    #
    # Read data.
    data_dict = {}
    for i_fn,fn in enumerate(filenames):
        ds = xr.open_dataset(fn[0], decode_times=False)
        data_dict[plot_names[i_fn]] = ds[v['ocn']][0,:,:]
    #
    # Mask data.
    ds_mask = xr.open_dataset(
        '/home/jackre/Data/maps/masks/ocean_mask_180x360.nc')
    mask_var = ds_mask.ocean_mask
    for k in list(data_dict.keys()):
        data_dict[k].data = mask_var.data*data_dict[k].data
    #
    # Return data.
    return(data_dict, plot_names)
    

################################################################################
# Functions to plot maps.
################################################################################

def plot_diffs(data_dict, name_list, var_name_dict, ssn, interp):
    """Plot maps.
    """
    #
    # Get variable for hatching.
    #sig_var = load_E3SM_std(var_name_dict,ssn)
    #sig_var = sig_var*(60.0*60.0*24.0)
    #sig_var.attrs['units'] = 'mm day-1'
    #
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['short_name'], ssn)
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 1),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axgr):
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[name_list[i]].coords['lat'],
                                 data_dict[name_list[i]].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(name_list[i],loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm,
                          data_dict[name_list[i]].data*\
                          plot_details['unit_conversion'] -
                          plot_details['offset'][name_list[i]],
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        #
        # Add hatching for significant values.
        #hatch_var = np.where(np.abs(data_dict[name_list[i]].data) >= sig_var,
        #                     1.0, 0.0)
        #hatch = ax.contourf(lonm, latm, hatch_var,
        #                    levels=np.array([-0.5,0.5,1.5]),
        #                    colors='none',
        #                    hatches=[None, '...'])
        #hatch = ax.contour(lonm, latm, hatch_var,
        #                   levels=np.array([-0.5,0.5,1.5]),
        #                   colors='black',
        #                   linewidths=0.3)
        #
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    #
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p)
    cb.set_label_text(plot_details['units'])
    #
    # Add over all details.
    fig.suptitle(plot_details['main_title'])
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + var_name_dict['short_name'] +
                '_' + ssn +
                '_' + interp +
                '.png',
                format='png')

    return()

def plot_info(v,s):
    """Give some details for plotting, based on variable name.
    Returns a dictionary with units, min and max for color scale.
    """
    plot_info_dict = {}
    # Season-specific bits.
    if s == 'ANN':
        s_string = 'annual mean '
    else:
        s_string = s + ' mean '
    # General details.
    if v == 'SSH':
        plot_info_dict['units'] = r'$cm$'
        plot_info_dict['unit_conversion'] = 100.0 # converts m to cm
        plot_info_dict['main_title'] = s_string + \
                                       'SSH in year 10'
        plot_info_dict['min'] = -8.0      
        plot_info_dict['max'] = 8.0       
        plot_info_dict['color_step'] = 1.0
        plot_info_dict['offset'] = {'ocean: UA - control':(-7.60),
                                    'ocean: COARE - control':(32.12),
                                    'ocean: COARE - UA':(39.72)}
    else:
        sys.exit('Variable name not valid - must be one of (SSH).')
    return(plot_info_dict)

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
