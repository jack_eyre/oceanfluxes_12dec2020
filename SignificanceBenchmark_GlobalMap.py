"""
Plot global maps of means for different algorithms for atmo and ocean runs.

Call as:
    $ python MeanComparison_Global.py var season
with command line arguments:
    var -- the variable: LHFLX, SHFLX, TAUX, TAUY
    season -- the averaging season: ANN, DJF, MAM, JJA, SON 
All arguments are required and the script fails without them.

Layout of plot panels:
    -----------------------------------------------------
    |  1. piControl: stdev    |  2. piControl: range    |
    -----------------------------------------------------
    |  3. OAflux: stdev       |  4. OAflux: range       |
    -----------------------------------------------------

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 3:
        sys.exit("Incorrect number of command line arguments (2 required)")
    else:
        var_names = parse_var(argv[1])
        season = check_season(argv[2])
    #
    # Load data.
    print('\nReading data...\n')
    plot_data = {}
    plot_data['E3SM piControl std. dev.'] = load_E3SM_std(var_names, season)
    plot_data['E3SM piControl range'] = load_E3SM_range(var_names, season)
    if (var_names['atmo'] == 'LHFLX') | (var_names['atmo'] == 'SHFLX'):
        plot_data['OAflux std. dev.'] = load_OAflux_std(var_names, season)
        plot_data['OAflux range'] = load_OAflux_range(var_names, season)
    #
    # Mask data.
    ds_mask = xr.open_dataset(
        '/home/jackre/Data/maps/masks/ocean_mask_180x360.nc')
    mask_var = ds_mask.ocean_mask
    for k in list(plot_data.keys()):
        plot_data[k].data = mask_var.data*plot_data[k].data
    #
    # Plot maps.
    print('\nPlotting...\n')
    plot_maps(plot_data, var_names, season)
    #
    return()

################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'LHFLX':
        var_name_dict['atmo'] = 'LHFLX'
        var_name_dict['ocn'] = 'timeMonthly_avg_latentHeatFlux'
        var_name_dict['CMIP'] = 'hfls'
        var_name_dict['OAflux'] = 'lhtfl'
    elif v == 'SHFLX':
        var_name_dict['atmo'] = 'SHFLX'
        var_name_dict['ocn'] = 'timeMonthly_avg_sensibleHeatFlux'
        var_name_dict['CMIP'] = 'hfss'
        var_name_dict['OAflux'] = 'shtfl'
    elif v == 'TAUX':
        var_name_dict['atmo'] = 'TAUX'
        var_name_dict['ocn'] = 'timeMonthly_avg_windStressZonal'
        var_name_dict['CMIP'] = 'tauu'
    elif v == 'TAUY':
        var_name_dict['atmo'] = 'TAUY'
        var_name_dict['ocn'] = 'timeMonthly_avg_windStressMeridional'
        var_name_dict['CMIP'] = 'tauv'
    else:
        sys.exit('Variable name not valid - must be one of (LHFLX, SHFLX, TAUX, TAUY).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'MAM', 'JJA', 'SON']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['conserve', 'bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (conserve, bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_E3SM_std(v,s):
    """Loads E3SM standard deviation data for plot.
    """
    print('\nReading E3SM data for standard deviation calculation...\n')
    # Define file names and data types:
    data_dir = '/home/jackre/Data/E3SM/piControl/atm/hist/'
    filenames = glob.glob(data_dir +
                          v['CMIP'] +
                          '_Amon_E3SM-1-0_piControl_r1i1p1f1_gr_*.nc')
    #
    # Read data.
    ds = xr.open_mfdataset(filenames)
    ts = ds[v['CMIP']]
    #
    # Calculate annual time series for given season.
    ann_ts = weighted_seasonal_mean(ts, s)
    #
    # Calculate output variable.
    std_map = ann_ts.std(dim='season_year')
    #
    # Return data.
    return(std_map)

def load_E3SM_range(v,s):
    """Loads E3SM range data for plot.
    """
    print('\nReading E3SM data for range calculation...\n')
    # Define file names and data types:
    data_dir = '/home/jackre/Data/E3SM/piControl/atm/hist/'
    filenames = glob.glob(data_dir +
                          v['CMIP'] +
                          '_Amon_E3SM-1-0_piControl_r1i1p1f1_gr_*.nc')
    #
    # Read data.
    ds = xr.open_mfdataset(filenames)
    ts = ds[v['CMIP']]
    #
    # Calculate series of 5-year means for given season.
    clim_series = weighted_seasonal_clim_series(ts, s, 5)
    #
    # Calculate output variable.
    range_map = clim_series.max(dim='year') - clim_series.min(dim='year')
    #
    # Return data.
    return(range_map)

def load_OAflux_std(v,s):
    """Loads OAflux standard deviation data for plot.
    """
    print('\nReading OAflux data for standard deviation calculation...\n')
    # Define file names and data types:
    data_dir = '/home/jackre/Data/ocean_obs/OAflux/'
    filename = v['OAflux'] + '_oaflux_1958-2018.nc'
    #
    # Read data.
    ds = xr.open_dataset(data_dir + filename)
    ts = ds[v['OAflux']]
    #
    # Calculate annual time series for given season.
    ann_ts = weighted_seasonal_mean(ts, s)
    #
    # Calculate output variable.
    std_map = ann_ts.std(dim='season_year')
    #
    # Return data.
    return(std_map)

def load_OAflux_range(v,s):
    """Loads OAflux range data for plot.
    """
    print('\nReading OAflux data for range calculation...\n')
    # Define file names and data types:
    data_dir = '/home/jackre/Data/ocean_obs/OAflux/'
    filename = v['OAflux'] + '_oaflux_1958-2018.nc'
    #
    # Read data.
    ds = xr.open_dataset(data_dir + filename)
    ts = ds[v['OAflux']]
    #
    # Calculate series of 5-year means for given season.
    clim_series = weighted_seasonal_clim_series(ts, s, 5)
    #
    # Calculate output variable.
    range_map = clim_series.max(dim='year') - clim_series.min(dim='year')
    #
    # Return data.
    return(range_map)

def mask_data(data_dict, mask_var_name, thresh):
    """Masks all items in data_dict to the same missing data mask.
    The missing data mask is calculated from the entry specified 
    by mask_var_name. Missing data are defined as those with absolute 
    value smaller than thresh.
    """
    #
    # Calculate mask.
    mask_var = np.where(np.isnan(data_dict[mask_var_name].data),
                        np.nan, 1.0)
    #
    # Loop over variables and mask.
    for k in data_dict.keys():
        data_dict[k].data = mask_var*data_dict[k].data
    #
    return(data_dict)
    
def weighted_seasonal_mean(ts, ssn):
    """Simple function to return seasonal mean.

    Input is an xarray data array with a time coordinate.

    Output is another xarray data array with one
    data point per year.

    Averages are weighted by days per month.
    
    DJF are seasonally continuous (in NCO terminology), so
    DJF of year y contains Dec of year y-1 and 
    Jan and Feb of year of year y. 
    """
    #
    # Define days per month (E3SM uses noleap calendar, thankfully!).
    month_length = xr.DataArray(get_dpm(ts.time, calendar='noleap'),
                                coords=[ts.time], name='month_length')
    #
    # Calculate weighted time series.
    ts_w = ts*month_length
    #
    # Get month and year
    months = ts.time.dt.month
    years = ts.time.dt.year
    #
    # Get alternative years to pick out required season.
    if ssn == 'ANN':
        ssn_years = years
        count_req = 12
    elif ssn == 'DJF':
        ssn_years = years.copy() + 3000
        ssn_years[months == 1] = years[months == 1]
        ssn_years[months == 2] = years[months == 2]
        ssn_years[months == 12] = years[months == 12]+1
        count_req = 3
    elif ssn == 'MAM':
        ssn_years = years.copy() + 3000
        ssn_years[months == 3] = years[months == 3]
        ssn_years[months == 4] = years[months == 4]
        ssn_years[months == 5] = years[months == 5]
        count_req = 3
    elif ssn == 'JJA':
        ssn_years = years.copy() + 3000
        ssn_years[months == 6] = years[months == 6]
        ssn_years[months == 7] = years[months == 7]
        ssn_years[months == 8] = years[months == 8]
        count_req = 3
    elif ssn == 'SON':
        ssn_years = years.copy() + 3000
        ssn_years[months == 9] = years[months == 9]
        ssn_years[months == 10] = years[months == 10]
        ssn_years[months == 11] = years[months == 11]
        count_req = 3
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    #
    # Add new coordinate and groupby it.
    ts_w.coords['season_year'] = ssn_years
    month_length.coords['season_year'] = ssn_years
    output = ts_w.groupby('season_year').sum(dim='time')/\
             month_length.groupby('season_year').sum(dim='time')
    output.load()
    #
    # Mask out values where not enough months.
    month_count = month_length.groupby('season_year').count(dim='time')
    output[month_count != count_req,:,:] = np.nan
    #
    return output

    
def weighted_seasonal_clim_series(ts, ssn, clim_len):
    """Simple function to return seasonal mean.

    Input is an xarray data array with a time coordinate.

    Output is another xarray data array with one
    data point for each non-overlapping climatology of length clim_len.

    Averages are weighted by days per month.
    
    DJF are seasonally discontinuous (in NCO terminology), so
    DJF for period 1 to 5 uses Dec from years 1,2,3,4,5.
    """
    #
    # Define days per month (E3SM uses noleap calendar, thankfully!).
    month_length = xr.DataArray(get_dpm(ts.time, calendar='noleap'),
                                coords=[ts.time], name='month_length')
    #
    # Calculate weighted time series.
    ts_w = ts*month_length
    #
    # Get month and year arrays.
    months = ts.time.dt.month
    years = ts.time.dt.year
    #
    # Get required number of months for season.
    if ssn == 'ANN':
        count_req = 12*clim_len
    else:
        count_req = 3*clim_len
    #
    # Set up data array to hold answers.
    st_yrs = np.arange(min(years),max(years),clim_len)
    end_yrs = st_yrs + clim_len - 1
    output = xr.DataArray(np.nan*ts[0:len(st_yrs),:,:],
                           coords={'year':st_yrs,
                                   'lat':ts.coords['lat'],
                                   'lon':ts.coords['lon'],
                                   'st_yr':('year',st_yrs),
                                   'end_yr':('year',end_yrs)},
                           dims=['year','lat','lon'])
    #
    # Get list of months for this season.
    season_list = {'DJF':[1,2,12], 'MAM':[3,4,5],
                   'JJA':[6,7,8], 'SON':[9,10,11]}
    #
    # Loop over climatology periods and take average.
    for t,sy in enumerate(st_yrs):
        if ssn == 'ANN':
            sub_period = (years >= st_yrs[t]) & (years <= end_yrs[t])
        else:
            sub_period = (years >= st_yrs[t]) & (years <= end_yrs[t]) & \
                         ((months == season_list[ssn][0]) | \
                          (months == season_list[ssn][1]) | \
                          (months == season_list[ssn][2]))
        if np.sum(sub_period.data) == count_req:  
            output[t,:,:] = ts_w[sub_period,:,:].sum(dim='time')/\
                            month_length[sub_period].sum(dim='time')
    #
    return output


########################################
# Weighted seasonal mean utilities from:
# http://xarray.pydata.org/en/v0.10.9/examples/monthly-means.html
#
def leap_year(year, calendar='standard'):
    """Determine if year is a leap year"""
    leap = False
    if ((calendar in ['standard', 'gregorian',
        'proleptic_gregorian', 'julian']) and
        (year % 4 == 0)):
        leap = True
        if ((calendar == 'proleptic_gregorian') and
            (year % 100 == 0) and
            (year % 400 != 0)):
            leap = False
        elif ((calendar in ['standard', 'gregorian']) and
                 (year % 100 == 0) and (year % 400 != 0) and
                 (year < 1583)):
            leap = False
    return leap

#
def get_dpm(time, calendar='standard'):
    """
    return an array of days per month corresponding to the 
    months provided in time.dt.month
    """
    dpm = {'noleap': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
           '365_day': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
           'standard': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
           'gregorian': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
           'proleptic_gregorian': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
           'all_leap': [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
           '366_day': [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
           '360_day': [0, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30]}
    cal_days = dpm[calendar]
    month_length = np.zeros(len(time), dtype=np.int)
    for i, (month, year) in enumerate(zip(time.dt.month, time.dt.year)):
        month_length[i] = cal_days[int(month)]
        if leap_year(year, calendar=calendar):
            month_length[i] += 1
    return month_length
########################################
#
################################################################################
# Functions to plot maps.
################################################################################

def plot_maps(data_dict, var_name_dict, ssn):
    """Plot maps.
    """
    #
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['atmo'], ssn)
    if (var_name_dict['atmo'] == 'LHFLX') | (var_name_dict['atmo'] == 'SHFLX'):
        n_cols = 2
    else:
        n_cols = 1
    n_rows = 2
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(n_rows, n_cols),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    d_k = list(data_dict.keys())
    for i, ax in enumerate(axgr):
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[d_k[i]].coords['lat'],
                                 data_dict[d_k[i]].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(d_k[i],loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm, data_dict[d_k[i]].data,
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
     
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    #
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p)
    cb.set_label_text(plot_details['units'])
    #
    # Add over all details.
    fig.suptitle(plot_details['main_title'])
    #
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + var_name_dict['atmo'] +
                '_' + ssn +
                '.png',
                format='png')

    return()

def plot_info(v,s):
    """Give some details for plotting, based on variable name.
    Returns a dictionary with units, min and max for color scale.
    """
    # Format season stuff first.
    if s == 'ANN':
        s_string = 'annual mean '
    else:
        s_string = s + ' mean '
    plot_info_dict = {}
    if v == 'LHFLX':
        plot_info_dict['units'] = r'$W\ m^{-2}$'
        plot_info_dict['min'] = -25.0
        plot_info_dict['max'] = 25.0
        plot_info_dict['color_step'] = 5.0
        plot_info_dict['main_title'] = s_string + \
                                       'latent heat flux (positive into ocean)'
    elif v == 'SHFLX':
        plot_info_dict['units'] = r'$W\ m^{-2}$'
        plot_info_dict['min'] = -10.0
        plot_info_dict['max'] = 10.0
        plot_info_dict['color_step'] = 2.0
        plot_info_dict['main_title'] = s_string + \
                                       'sensible heat flux (positive into ocean)'
    elif v == 'TAUX':
        plot_info_dict['units'] = r'$N\ m^{-2}$'
        plot_info_dict['min'] = -0.05
        plot_info_dict['max'] = 0.05
        plot_info_dict['color_step'] = 0.01
        plot_info_dict['main_title'] = s_string + \
                                       'zonal wind stress (positive eastward)'
    elif v == 'TAUY':
        plot_info_dict['units'] = r'$N\ m^{-2}$'
        plot_info_dict['min'] = -0.02
        plot_info_dict['max'] = 0.02
        plot_info_dict['color_step'] = 0.004
        plot_info_dict['main_title'] = s_string +  \
                                       'meridional wind stress (positive northward)'
    else:
        sys.exit('Variable name not valid - must be one of (LHFLX, SHFLX, TAUX, TAUY).')
    return(plot_info_dict)

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
