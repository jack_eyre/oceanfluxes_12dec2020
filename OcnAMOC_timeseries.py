"""
Plot time series of Atlantic meridional overturning circulation from different 
model runs. 

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import pandas as pd
import datetime
import cf_units
import calendar
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

def main():
    #
    # Load data (get 12 month moving average).
    print('-----\nReading data...\n-----')
    MOC_data,run_names = load_MOC(12)
    #
    # Plot results.
    print('-----\nPlotting...\n-----')
    plot_timeseries(MOC_data,run_names)
    #
    return

################################################################################
# Functions to load and process data.
################################################################################


def load_MOC(n_points):
    #
    # Define cases.
    cases = ['20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil',
             '20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil',
             '20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil']
    case_names = ['control',
                  'UA',
                  'COARE']
    #
    # Initialize variable to hold data.
    data_dict = {}
    # Load files.
    for ic,c in enumerate(cases):
        file_name = '~/Data/E3SM/' + c + '/ocn/' + \
                    'mocTimeSeries_0001-0010.nc'
        ds = xr.open_dataset(file_name)
        data_dict[case_names[ic]] = \
            running_mean(ds['mocAtlantic26'],n_points)
    #
    return data_dict,case_names


def running_mean(ts, npoints):
    #
    # Routine from mpas_analysis:
    # https://github.com/MPAS-Dev/MPAS-Analysis/blob/c377a327d13a3fc2ac7af4fb25bf1da3ec48cb25/mpas_analysis/shared/plot/time_series.py#L133.
        if npoints == 1 or npoints is None:
            mean = ts
        else:
            mean = pd.Series.rolling(ts.to_pandas(), npoints,
                                     center=True).mean()
            mean = xr.DataArray.from_series(mean)
        #
        return mean

################################################################################
# Functions to plot data.
################################################################################

def plot_timeseries(data_dict, data_names):
    #
    # Define colors.
    custom_cols = ['#268bd2',
                   '#6c71c4',
                   '#dc322f',
                   '#b58900']
    #
    # Set up panel and axes.
    fig,ax = plt.subplots(1,1,
                          figsize=(8,4))
    #
    #
    # Plot the model data.
    for i_c, c in enumerate(data_names):
        year = data_dict[c].coords['Time'].dt.year.data
        month = data_dict[c].coords['Time'].dt.month.data
        year_month = ["{:0>4d}".format(year[i]) + '-' +
                      "{:0>2d}".format(month[i]) for i in range(0,len(year))]
        ax.plot(range(len(year)),
                data_dict[c],
                color=custom_cols[i_c],
                label=c)
    #
    # Plot details.
    ax.set_xlim(0,len(year))
    ax.set_ylim(10.0,18.0)
    ax.set_ylabel('AMOC at 26.5' + r'$^{\circ}N$' + ' / ' + r'$Sv$')
    ax.set_xlabel('time / yyyy-mm')
    ax.xaxis.set_tick_params(which='both',
                             bottom=True,top=True)
    ax.yaxis.set_tick_params(which='both',
                             left=True,right=True)
    ax.set_xticks(range(0,len(year),12))
    ax.set_xticklabels(year_month[0::12],rotation=45)
    fig.subplots_adjust(bottom=0.3)
    #
    # Add legend.
    ax.legend(loc='upper right')
    #      
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    #
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Show the plot.
    #plt.show()
    # Save the figure.
    plt.savefig(os.path.expanduser('~') +
                '/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')
    #
    return

    
################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
