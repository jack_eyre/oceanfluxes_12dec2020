"""
Plot global maps of fluxes for different years to look at interannual variability.

Call as:
    $ python InterannualVariability_GlobalMap.py var season interp model s e
with command line arguments:
    var -- the variable: LHFLX, SHFLX, TAUX, TAUY
    season -- the averaging season: ANN, DJF, MAM, JJA, SON 
    interp -- the interpolation method: conserve, bilin
    model -- the model component to look at: atmo, ocn
    s -- start year
    e -- end year
All arguments are required and the script fails without them.

Layout of plot panels:
    ------------------------------------------
    |  Year 1. ctl    |  ... | Year 1. COARE |
    ------------------------------------------
    |  Year 2. ctl    |  ... | Year 2. COARE |
    ------------------------------------------
    |  ...            |  ... | ...           |
    ------------------------------------------
    |  Year 5. ctl    |  ... | Year 5. COARE |
    ------------------------------------------

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
import cftime
# For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
# Custom functions from other scripts.
import MeanComparison_Global
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 7:
        sys.exit("Incorrect number of command line arguments (3 required)")
    else:
        var_names = parse_var(argv[1])
        season = check_season(argv[2])
        interp = check_interp(argv[3])
        model = check_model(argv[4])
        st_yr = np.float(argv[5])
        end_yr = np.float(argv[6])
    #
    # Load annual data.
    if model == 'atmo':
        plot_data,plot_names = load_atmo_data(var_names, season, interp,
                                              st_yr, end_yr)
    elif model == 'ocn':
        plot_data,plot_names = load_ocn_data(var_names, season, interp,
                                             st_yr, end_yr)
        
        #
    # Plot maps.
    #plot_maps(plot_data, plot_names, var_names, season, interp, 
    #          model, np.int64(st_yr), np.int64(end_yr))
    plot_sign_maps(plot_data, plot_names, var_names, season, interp, 
                   model, np.int64(st_yr), np.int64(end_yr))
    #
    return()

################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'LHFLX':
        var_name_dict['atmo'] = 'LHFLX'
        var_name_dict['ocn'] = 'timeMonthly_avg_latentHeatFlux'
    elif v == 'SHFLX':
        var_name_dict['atmo'] = 'SHFLX'
        var_name_dict['ocn'] = 'timeMonthly_avg_sensibleHeatFlux'
    elif v == 'TAUX':
        var_name_dict['atmo'] = 'TAUX'
        var_name_dict['ocn'] = 'timeMonthly_avg_windStressZonal'
    elif v == 'TAUY':
        var_name_dict['atmo'] = 'TAUY'
        var_name_dict['ocn'] = 'timeMonthly_avg_windStressMeridional'
    else:
        sys.exit('Variable name not valid - must be one of (LHFLX, SHFLX, TAUX, TAUY).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'MAM', 'JJA', 'SON']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['conserve', 'bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (conserve, bilin).')
    return(interp)
      
def check_model(m):
    """Check for valid model component.
    """
    if m in ['atmo', 'ocn']:
        model = m
    else:
        sys.exit('Model component not valid - must be one of (atmo).')
    return(model)

################################################################################
# Functions to load and process data.
################################################################################

def load_atmo_data(v,s,i, sy, ey):
    """Loads atmosphere model data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    # Define file names and data types:
    ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/hist/180x360_' + i + '/'
    UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/hist/180x360_' + i + '/'
    COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/hist/180x360_' + i + '/'
    filename_wildcards = [ctl_a_dir +
                          '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl.cam' +
                          '.h0.*.nc',
                          UA_a_dir +
                          '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl.cam' +
                          '.h0.*.nc',
                          COARE_a_dir +
                          '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl.cam' +
                          '.h0.*.nc']
    #
    # Define plot titles based on above files.
    annual_names = ['control',
                    'UA',
                    'COARE']
    #
    # Read data.
    annual_dict = {}
    for i_fn,fn in enumerate(filename_wildcards):
        print(fn)
        ds = xr.open_mfdataset(fn, concat_dim="time")
        # This puts time variable as end of month - change it to beginning.
        new_time_data = ds.time_bnds[:,0].load()
        new_time_ds = xr.Dataset({'time':
                                  ('time', new_time_data,
                                   {'units':'days since 0001-01-01 00:00:00',
                                    'calendar':'noleap'})})
        ds = ds.assign_coords(time=xr.decode_cf(new_time_ds).coords['time'])
        # Change sign convention.
        print('Inverting atmo model variable to match ocean model sign convention.')
        var_all = -1.0*ds[v['atmo']]
        # Check time period.
        if (sy < min(var_all.time.dt.year) or ey > max(var_all.time.dt.year)):
            print('Requested period greater than available period')
            print('Requested: ' + str(sy) + ' - ' + str(ey))
            print('Available: ' + str(min(var_all.time).data) +
                  ' - ' + str(max(var_all.time).data))
        # Average over requested season.
        var_ssn = seasonal_mean(var_all, s, sy, ey)
        # Add data to dictionary.
        annual_dict[annual_names[i_fn]] = var_ssn
    #
    # Get clim data from other script (this also handles the masking).
    clim_data,clim_names = MeanComparison_Global.load_data(v,s,i)
    #
    # Subtract climatology data to match climatology difference plot:
    diff_names = ['atmosphere: UA - ctl',
                  'atmosphere: COARE - ctl',
                  'atmosphere: COARE - UA']
    diff_dict = {diff_names[0]: {},
                 diff_names[1]: {},
                 diff_names[2]: {}}
    diff_dict['atmosphere: UA - ctl']['data'] = annual_dict['UA'] - \
                                                clim_data['atmosphere: control']
    diff_dict['atmosphere: UA - ctl']['timeseries'] = 'UA'
    diff_dict['atmosphere: UA - ctl']['clim'] = 'ctl'
    diff_dict['atmosphere: COARE - ctl']['data'] = annual_dict['COARE'] - \
                                                   clim_data['atmosphere: control']
    diff_dict['atmosphere: COARE - ctl']['timeseries'] = 'COARE'
    diff_dict['atmosphere: COARE - ctl']['clim'] = 'ctl'
    diff_dict['atmosphere: COARE - UA']['data'] = annual_dict['COARE'] - \
                                                  clim_data['atmosphere: UA']
    diff_dict['atmosphere: COARE - UA']['timeseries'] = 'COARE'
    diff_dict['atmosphere: COARE - UA']['clim'] = 'UA'
    #
    # Return data.
    return(diff_dict, diff_names)

def load_ocn_data(v,s,i, sy, ey):
    """Loads ocean model data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    # Define file names and data types:
    ctl_o_dir = '/home/jackre/Data/E3SM/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_' + i + '/'
    UA_o_dir = '/home/jackre/Data/E3SM/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_' + i + '/'
    COARE_o_dir = '/home/jackre/Data/E3SM/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_' + i + '/'
    filename_wildcards = [ctl_o_dir +
                          'mpaso.hist.am.timeSeriesStatsMonthly.' +
                          '*.nc',
                          UA_o_dir +
                          'mpaso.hist.am.timeSeriesStatsMonthly.' +
                          '*.nc',
                          COARE_o_dir +
                          'mpaso.hist.am.timeSeriesStatsMonthly.' +
                          '*.nc']
    #
    # Define plot titles based on above files.
    annual_names = ['control',
                    'UA',
                    'COARE']
    #
    # Read data.
    print('Getting time series data...')
    annual_dict = {}
    for i_fn,fn in enumerate(filename_wildcards):
        print(fn)
        single_filename_list = glob.glob(fn)
        da_list = []
        for sfn in single_filename_list:
            ds_1 = xr.open_dataset(sfn)
            ds_1.attrs = {'all':'deleted'}
            new_time = cftime.DatetimeNoLeap(int(sfn.split('.')[-2].split('-')[0]),
                                            int(sfn.split('.')[-2].split('-')[1]),
                                            int(sfn.split('.')[-2].split('-')[2]))
            da_new = xr.DataArray(ds_1[v['ocn']].data,
                                  coords=[[new_time],
                                          ds_1.coords['lat'],
                                          ds_1.coords['lon']],
                                  dims=['time','lat','lon'])
            da_list.append(da_new)
        ds = xr.concat(da_list, dim='time')
        # Reorder to ascending time.
        var_all = ds.sortby(ds.time)
        # Check time period.
        if (sy < min(var_all.time.dt.year) or ey > max(var_all.time.dt.year)):
            print('Requested period greater than available period')
            print('Requested: ' + str(sy) + ' - ' + str(ey))
            print('Available: ' + str(min(var_all.time).data) +
                  ' - ' + str(max(var_all.time).data))
        # Average over requested season.
        var_ssn = seasonal_mean(var_all, s, sy, ey)
        # Add data to dictionary.
        annual_dict[annual_names[i_fn]] = var_ssn
    #
    # Get clim data from other script (this also handles the masking).
    print('Getting climatology data...')
    clim_data,clim_names = MeanComparison_Global.load_data(v,s,i)
    #
    # Subtract climatology data to match climatology difference plot:
    diff_names = ['ocean: UA - ctl',
                  'ocean: COARE - ctl',
                  'ocean: COARE - UA']
    diff_dict = {diff_names[0]: {},
                 diff_names[1]: {},
                 diff_names[2]: {}}
    diff_dict['ocean: UA - ctl']['data'] = annual_dict['UA'] - \
                                           clim_data['ocean: control']
    diff_dict['ocean: UA - ctl']['timeseries'] = 'UA'
    diff_dict['ocean: UA - ctl']['clim'] = 'ctl'
    diff_dict['ocean: COARE - ctl']['data'] = annual_dict['COARE'] - \
                                              clim_data['ocean: control']
    diff_dict['ocean: COARE - ctl']['timeseries'] = 'COARE'
    diff_dict['ocean: COARE - ctl']['clim'] = 'ctl'
    diff_dict['ocean: COARE - UA']['data'] = annual_dict['COARE'] - \
                                             clim_data['ocean: UA']
    diff_dict['ocean: COARE - UA']['timeseries'] = 'COARE'
    diff_dict['ocean: COARE - UA']['clim'] = 'UA'
    #
    # Return data.
    return(diff_dict, diff_names)

def seasonal_mean(ts, ssn, sty, endy):
    """Simple function to return seasonal mean with NaN value if not
    all months are present for each season.
    Input is an xarray data array with a time coordinate.
    Output is another xarray data array with time averaged to one
    data point per season.
    Averages are not currently weighted by days per month.
    This should be quite simple to change due to the "noleap" calendar.
    """
    #
    # Get month and year
    months = ts.time.dt.month
    years = ts.time.dt.year
    #
    # Get alternative years to pick out required season.
    if ssn == 'ANN':
        ssn_years = years
    elif ssn == 'DJF':
        ssn_years = years.copy() + 3000
        ssn_years[months == 1] = years[months == 1]
        ssn_years[months == 2] = years[months == 2]
        ssn_years[months == 12] = years[months == 12]+1
    elif ssn == 'MAM':
        ssn_years = years.copy() + 3000
        ssn_years[months == 3] = years[months == 3]
        ssn_years[months == 4] = years[months == 4]
        ssn_years[months == 5] = years[months == 5]
    elif ssn == 'JJA':
        ssn_years = years.copy() + 3000
        ssn_years[months == 6] = years[months == 6]
        ssn_years[months == 7] = years[months == 7]
        ssn_years[months == 8] = years[months == 8]
    elif ssn == 'SON':
        ssn_years = years.copy() + 3000
        ssn_years[months == 9] = years[months == 9]
        ssn_years[months == 10] = years[months == 10]
        ssn_years[months == 11] = years[months == 11]
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    #
    # Add new coordinate and groupby it, average it.
    ts.coords['season_year'] = ssn_years
    ts_a = ts.groupby('season_year').mean(dim='time')
    output = ts_a.sel(season_year=slice(sty,endy))
    #
    return output
    

################################################################################
# Functions to plot maps.
################################################################################

def plot_maps(data_dict, name_list, var_name_dict, ssn, interp,
              model, sy, ey):
    """Plot maps.
    """
    #
    print('Plotting...')
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['atmo'], ssn)
    #
    # Get number of rows and columns.
    n_rows = 1 + ey - sy
    n_cols = len(name_list)
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(n_rows, n_cols),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axgr):
        #
        i_model = i%n_cols
        i_yr = np.int64((i - i_model)/n_cols)
        mname = name_list[i_model]
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[mname]['data'].coords['lat'],
                                 data_dict[mname]['data'].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(data_dict[mname]['clim'] + ' clim.',loc='right')
        ax.set_title(data_dict[mname]['timeseries'] + ' yr ' +
                     str(sy + i_yr),loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm, data_dict[mname]['data'].data[i_yr,:,:],
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p)
    cb.set_label_text(plot_details['units'])
    #
    # Add over all details.
    fig.suptitle(model + ' model ' + plot_details['main_title'])
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + model + 
                '_' + var_name_dict['atmo'] +
                '_' + ssn +
                '_' + interp +
                '_' + str(sy) + '-' + str(ey) +
                '.png',
                format='png')

    return()


def plot_sign_maps(data_dict, name_list, var_name_dict, ssn, interp,
                   model, sy, ey):
    """Plot maps of number of positive and negative changes to give
       an idea of consensus.
    """
    #
    print('Plotting sign maps...')
    #
    # Get plot details (just for title).
    plot_details = plot_info(var_name_dict['atmo'], ssn)
    #
    # Get number of rows and columns.
    n_rows = 1
    n_cols = len(name_list)
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(-0.5,
                                           1.51 + ey - sy,
                                           1),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(n_rows, n_cols),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axgr):
        #
        i_model = i
        mname = name_list[i_model]
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[mname]['data'].coords['lat'],
                                 data_dict[mname]['data'].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(data_dict[mname]['clim'] + ' clim.',loc='right')
        ax.set_title(data_dict[mname]['timeseries'] + ' interannual',
                     loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm,
                          np.where(np.isnan(data_dict[mname]['data'][0,:,:]),
                                   np.nan,
                                   (data_dict[mname]['data']>0.0).\
                                    sum(dim='season_year')),
                          vmin=-0.5,
                          vmax=1.5+ey-sy, 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p, ticks=np.arange(0,2+ey-sy))
    cb.set_label_text('count of positive years')
    #
    # Add over all details.
    fig.suptitle(model + ' model ' + plot_details['main_title'] +
                 '\n count of years with positive changes')
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_Sign_' + model + 
                '_' + var_name_dict['atmo'] +
                '_' + ssn +
                '_' + interp +
                '_' + str(sy) + '-' + str(ey) +
                '.png',
                format='png')

    return()


def plot_info(v,s):
    """Give some details for plotting, based on variable name.
    Returns a dictionary with units, min and max for color scale.
    """
    # Format season stuff first.
    if s == 'ANN':
        s_string = 'annual mean '
    else:
        s_string = s + ' mean '
    plot_info_dict = {}
    if v == 'LHFLX':
        plot_info_dict['units'] = r'$W\ m^{-2}$'
        plot_info_dict['min'] = -25.0
        plot_info_dict['max'] = 25.0
        plot_info_dict['color_step'] = 5.0
        plot_info_dict['main_title'] = s_string + \
                                       'latent heat flux (positive into ocean)'
    elif v == 'SHFLX':
        plot_info_dict['units'] = r'$W\ m^{-2}$'
        plot_info_dict['min'] = -10.0
        plot_info_dict['max'] = 10.0
        plot_info_dict['color_step'] = 2.0
        plot_info_dict['main_title'] = s_string + \
                                       'sensible heat flux (positive into ocean)'
    elif v == 'TAUX':
        plot_info_dict['units'] = r'$N\ m^{-2}$'
        plot_info_dict['min'] = -0.05
        plot_info_dict['max'] = 0.05
        plot_info_dict['color_step'] = 0.01
        plot_info_dict['main_title'] = s_string + \
                                       'zonal wind stress (positive eastward)'
    elif v == 'TAUY':
        plot_info_dict['units'] = r'$N\ m^{-2}$'
        plot_info_dict['min'] = -0.02
        plot_info_dict['max'] = 0.02
        plot_info_dict['color_step'] = 0.004
        plot_info_dict['main_title'] = s_string +  \
                                       'meridional wind stress (positive northward)'
    else:
        sys.exit('Variable name not valid - must be one of (LHFLX, SHFLX, TAUX, TAUY).')
    return(plot_info_dict)

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
