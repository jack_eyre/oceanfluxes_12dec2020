"""
Test of OPeNDAP access methods
===============

Don't forget to load anaconda environment before running this:
$ source activate plotenv
and you might also need to change environment variables:
$ export DYLD_FALLBACK_LIBRARY_PATH=${HOME}/lib:/usr/local/lib:/lib:/usr/lib

Then it should work....
"""

###########################################
import iris
import numpy as np
import datetime
from iris.time import PartialDateTime
###########################################

### Load data. ###

# Get sample 1x1 grid.
grid_1x1 = iris.load_cube('/Users/jameseyre/Data/ocean_obs/OAflux/evapr_oaflux_1981.nc', 'evapr' & iris.Constraint(time = 1))
grid_1x1.coord('longitude').guess_bounds()
grid_1x1.coord('latitude').guess_bounds()

# Opendap server URLs:
url = 'https://opendap.nccs.nasa.gov/dods/OSSE/G5NR/Ganymed/7km/0.0625_deg/tavg/tavg30mn_2d_met2_Nx'

# Specify which times are needed.
one_day = iris.Constraint(time=lambda tt: datetime.datetime(2006,6,15,0,1) < tt < datetime.datetime(2006,6,15,23,59))

# Load specific variables from server.
taux = iris.load(url, 'eastward_surface_stress ' & one_day)
tauy = iris.load(url, 'northward_surface_stress ' & one_day)
# May need to guess_bounds for these too.

print(cubes)

### Calculate wind stress magnitude then re-grid ###

# Combine to give wind stress magnitudes.
tau = (taux**2 + tauy**2)**0.5

#Re-grid to 1x1 lat-lon grid.
R_scheme = iris.analysis.AreaWeighted(mdtol=1.0)
tau_MR = tau.regrid(grid_1x1, R_scheme)

### Re-grid components then calculate wind stress magnitude ###
taux_R = taux.regrid(grid_1x1, R_scheme)
tauy_R = tauy.regrid(grid_1x1, R_scheme)
tau_RM = (taux_R**2 + tauy_R**2)**0.5

# Calculate difference between methods.
diff = tau_MR - tau_RM

### Plot maps of different methods and their difference ###


