"""
Plot global maps of means for different algorithms for atmo and ocean runs.

Call as:
    $ python MeanComparison_Global.py var season interp
with command line arguments:
    var -- the variable: LHFLX, SHFLX, TAUX, TAUY
    season -- the averaging season: ANN, DJF, MAM, JJA, SON 
    interp -- the interpolation method: conserve, bilin
All arguments are required and the script fails without them.

Layout of plot panels:
    ---------------------------------------
    |  1. atmo: ctl    |  2. ocn: ctl     |
    ---------------------------------------
    |  3. atmo: UA     |  4. ocn: UA      |
    ---------------------------------------
    |  5. atmo: COARE  |  6. ocn: COARE   |
    ---------------------------------------

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 4:
        sys.exit("Incorrect number of command line arguments (3 required)")
    else:
        var_names = parse_var(argv[1])
        season = check_season(argv[2])
        interp = check_interp(argv[3])
    #
    # Load data.
    plot_data,plot_names = load_data(var_names, season, interp)
    #
    # Plot maps.
    plot_maps(plot_data, plot_names, var_names, season, interp)
    #
    return()

################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'LHFLX':
        var_name_dict['atmo'] = 'LHFLX'
        var_name_dict['ocn'] = 'timeMonthly_avg_latentHeatFlux'
    elif v == 'SHFLX':
        var_name_dict['atmo'] = 'SHFLX'
        var_name_dict['ocn'] = 'timeMonthly_avg_sensibleHeatFlux'
    elif v == 'TAUX':
        var_name_dict['atmo'] = 'TAUX'
        var_name_dict['ocn'] = 'timeMonthly_avg_windStressZonal'
    elif v == 'TAUY':
        var_name_dict['atmo'] = 'TAUY'
        var_name_dict['ocn'] = 'timeMonthly_avg_windStressMeridional'
    else:
        sys.exit('Variable name not valid - must be one of (LHFLX, SHFLX, TAUX, TAUY).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'MAM', 'JJA', 'SON']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['conserve', 'bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (conserve, bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_data(v,s,i):
    """Loads data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    # Define file names and data types:
    ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/'
    ctl_o_dir = '/home/jackre/Data/E3SM/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/'
    UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/'
    UA_o_dir = '/home/jackre/Data/E3SM/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/'
    COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/'
    COARE_o_dir = '/home/jackre/Data/E3SM/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/180x360_' + i + '/'
    filenames = [glob.glob(ctl_a_dir +
                           '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(ctl_o_dir + 'mpaso_' +
                           s + '_*_climo.nc'),
                 glob.glob(UA_a_dir +
                           '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(UA_o_dir + 'mpaso_' +
                           s + '_*_climo.nc'),
                 glob.glob(COARE_a_dir +
                           '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.' + i + '.nc'),
                 glob.glob(COARE_o_dir + 'mpaso_' +
                           s + '_*_climo.nc')]
    data_types = ['atmo','ocn','atmo','ocn','atmo','ocn']
    #
    # Define plot titles based on above files.
    plot_names = ['atmosphere: control',
                  'ocean: control',
                  'atmosphere: UA',
                  'ocean: UA',
                  'atmosphere: COARE',
                  'ocean: COARE']
    #
    # Read data.
    data_dict = {}
    for i_fn,fn in enumerate(filenames):
        print(fn[0])
        ds = xr.open_dataset(fn[0], decode_times=False)
        if data_types[i_fn]=='atmo':
            print('Inverting atmo model variable to match ocean model sign convention.')
            data_dict[plot_names[i_fn]] = -1.0*ds[v[data_types[i_fn]]][0,:,:]
        else:
            data_dict[plot_names[i_fn]] = ds[v[data_types[i_fn]]][0,:,:]
    #
    # Mask the data.
    data_dict = mask_data(data_dict,'ocean: control',0.00001)
    #
    # Return data.
    return(data_dict, plot_names)

def mask_data(data_dict, mask_var_name, thresh):
    """Masks all items in data_dict to the same missing data mask.
    The missing data mask is calculated from the entry specified 
    by mask_var_name. Missing data are defined as those with absolute 
    value smaller than thresh.
    """
    #
    # Calculate mask.
    mask_var = np.where(np.isnan(data_dict[mask_var_name].data),
                        np.nan, 1.0)
    #
    # Loop over variables and mask.
    for k in data_dict.keys():
        data_dict[k].data = mask_var*data_dict[k].data
    #
    return(data_dict)
    

################################################################################
# Functions to plot maps.
################################################################################

def plot_maps(data_dict, name_list, var_name_dict, ssn, interp):
    """Plot maps.
    """
    #
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['atmo'], ssn)
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 2),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axgr):
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[name_list[i]].coords['lat'],
                                 data_dict[name_list[i]].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(name_list[i],loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm, data_dict[name_list[i]].data,
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p)
    cb.set_label_text(plot_details['units'])
    #
    # Add over all details.
    fig.suptitle(plot_details['main_title'])
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + var_name_dict['atmo'] +
                '_' + ssn +
                '_' + interp +
                '.png',
                format='png')

    return()

def plot_info(v,s):
    """Give some details for plotting, based on variable name.
    Returns a dictionary with units, min and max for color scale.
    """
    # Format season stuff first.
    if s == 'ANN':
        s_string = 'annual mean '
    else:
        s_string = s + ' mean '
    plot_info_dict = {}
    if v == 'LHFLX':
        plot_info_dict['units'] = r'$W\ m^{-2}$'
        plot_info_dict['min'] = -300.0
        plot_info_dict['max'] = 50.0
        plot_info_dict['color_step'] = 50.0
        plot_info_dict['main_title'] = s_string + \
                                       'latent heat flux (positive into ocean)'
    elif v == 'SHFLX':
        plot_info_dict['units'] = r'$W\ m^{-2}$'
        plot_info_dict['min'] = -100.0
        plot_info_dict['max'] = 10.0
        plot_info_dict['color_step'] = 10.0
        plot_info_dict['main_title'] = s_string + \
                                       'sensible heat flux (positive into ocean)'
    elif v == 'TAUX':
        plot_info_dict['units'] = r'$N\ m^{-2}$'
        plot_info_dict['min'] = -0.2
        plot_info_dict['max'] = 0.2
        plot_info_dict['color_step'] = 0.04
        plot_info_dict['main_title'] = s_string + \
                                       'zonal wind stress (positive eastward)'
    elif v == 'TAUY':
        plot_info_dict['units'] = r'$N\ m^{-2}$'
        plot_info_dict['min'] = -0.1
        plot_info_dict['max'] = 0.1
        plot_info_dict['color_step'] = 0.02
        plot_info_dict['main_title'] = s_string +  \
                                       'meridional wind stress (positive northward)'
    else:
        sys.exit('Variable name not valid - must be one of (LHFLX, SHFLX, TAUX, TAUY).')
    return(plot_info_dict)

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
