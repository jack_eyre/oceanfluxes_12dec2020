"""
Download OAflux sensible and latent heat fluxes.
===============
"""

###########################################
import numpy as np
import datetime
import calendar
import urllib.request
###########################################

# Specify data location details.
save_dir = '/home/jackre/Data/ocean_obs/OAflux/'
url_base = 'ftp://ftp.whoi.edu/pub/science/oaflux/data_v3/monthly/turbulence/'
filename_head_lh = 'lh_oaflux_'
filename_head_sh = 'sh_oaflux_'
filename_tail = '.nc.gz'

# Specify dates of available data.
time_min = 1958
time_max = 2018

for y in range(time_min, time_max+1):
    #
    print(y)
    #
    filename_lh = filename_head_lh + "{:04d}".format(y) + filename_tail
    filename_sh = filename_head_sh + "{:04d}".format(y) + filename_tail
    #
    # Do the download.
    urllib.request.urlretrieve(url_base + filename_sh,
                               save_dir + filename_sh)
    urllib.request.urlretrieve(url_base + filename_lh,
                               save_dir + filename_lh)

