"""
Calculate global means from E3SM Atmosphere Model output.
Run using E3SM unified environment on Cori.
"""

###########################################
import numpy as np
import xarray as xr
from itertools import product
from cftime import DatetimeNoLeap
import datetime
import calendar
import os
import sys
import subprocess
###########################################


def main():
    #
    # Get case name.
    case_name = sys.argv[1]
    print(case_name)
    print('Setting up...')
    #
    # Data locations.
    data_dir = '/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/' +\
               case_name + '/atm/hist/'
    save_dir = '/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/' +\
               case_name + '/atm/'
    #
    # Filenames.
    filename_root = case_name + '.cam.h0.'
    filename_tail = '.nc'
    new_filename = case_name + '_EAM_customGlobalStats.nc'
    #
    # Specify time period and create date/time array.
    st_yr = 1
    end_yr = 6
    datelist = [DatetimeNoLeap(year, month, 1) for year, month in
                product(range(st_yr, end_yr+1), range(1, 13))]
    dates = xr.DataArray(datelist, coords=[datelist], dims='time')
    #
    # Specify variables to extract - these will be averaged over ocean.
    avg_vars = ['LHFLX',
                'SHFLX',
                'TAUX',
                'TAUY',
                'FSNS',
                'FSDS',
                'FLNS',
                'FLDS',
                'QFLX',
                'TREFHT',
                'QREFHT',
                'U10']
    # These will be averaged globally (land + ocean)
    glb_vars = ['PRECT_GLOBAL']
    # A few others will be done manually to combine them.
    man_vars = ['PRECT_OCEAN',
                'FTNS',
                'TSDIFF']
    #
    # Create dictionaries and data arrays to hold results.
    avg_data = {}
    for v in avg_vars:
        avg_data[v] = array_init(dates, v)
    glb_data = {}
    for v in glb_vars:
        glb_data[v] = array_init(dates, v)
    man_data = {}
    for v in man_vars:
        man_data[v] = array_init(dates, v)
    #
    # Loop over time (one file per month).
    print('Reading data...')
    load_attrs = True
    for yy in range(st_yr,end_yr+1):
        for mm in range(1,13):
            #
            # Get time index of this month.
            iym = (dates.dt.year == yy) & (dates.dt.month == mm)
            #
            # Load file.
            filename =  data_dir + filename_root + \
                        "{:04d}".format(yy) + '-' + \
                        "{:02d}".format(mm) + \
                        filename_tail
            print(filename)
            ds = xr.open_dataset(filename)
            #
            # Get area and landmask (make global so we don't have to pass it
            # through the function call every time).
            global area
            area = ds['area']
            global ocnfrac
            ocnfrac = 1.0 - ds['LANDFRAC']
            #
            # Read 2D data and average.
            for v in avg_vars:
                field = ds[v].isel(time=0)
                avg_data[v][iym] = oceanAverage(field)
                #((field*area_2D).sum()/area_sum).data
            #
            # Load attributes on first loop.
            if load_attrs:
                for v in avg_vars:                  
                    avg_data[v].attrs = ds[v].attrs
                    avg_data[v].attrs['area_methods'] = 'Global ocean mean: grid cells weighted by area and non-land fraction'
                load_attrs = False
            #
            # Do manual variables.
            field = ds['PRECL'].isel(time=0) + ds['PRECC'].isel(time=0)
            glb_data['PRECT_GLOBAL'][iym] = globalAverage(field)
            glb_data['PRECT_GLOBAL'].attrs = ds['PRECL'].attrs
            glb_data['PRECT_GLOBAL'].attrs['long_name'] = 'Total precipitation rate (liq + ice)'
            glb_data['PRECT_GLOBAL'].attrs['area_methods'] = 'Global mean: grid cells weighted by area'
            #
            field = ds['PRECL'].isel(time=0) + ds['PRECC'].isel(time=0)
            man_data['PRECT_OCEAN'][iym] = oceanAverage(field)
            man_data['PRECT_OCEAN'].attrs = ds['PRECL'].attrs
            man_data['PRECT_OCEAN'].attrs['long_name'] = 'Total precipitation rate (liq + ice)'
            man_data['PRECT_OCEAN'].attrs['area_methods'] = 'Global ocean mean: grid cells weighted by area and non-land fraction'
            #
            # CHECK SIGN CONVENTIONS!!!
            field = ds['FSNS'].isel(time=0) - ds['FLNS'].isel(time=0) - \
                    ds['LHFLX'].isel(time=0) - ds['SHFLX'].isel(time=0)
            man_data['FTNS'][iym] = oceanAverage(field)
            man_data['FTNS'].attrs = ds['FSNS'].attrs
            man_data['FTNS'].attrs['long_name'] = 'Net heat flux at surface'
            man_data['FTNS'].attrs['area_methods'] = 'Global ocean mean: grid cells weighted by area and non-land fraction'
            #
            field = ds['TREFHT'].isel(time=0) - ds['TS'].isel(time=0)
            man_data['TSDIFF'][iym] = oceanAverage(field)
            man_data['TSDIFF'].attrs = ds['TS'].attrs
            man_data['TSDIFF'].attrs['long_name'] = 'Reference height temperature minus surface temperature'
            man_data['TSDIFF'].attrs['area_methods'] = 'Global ocean mean: grid cells weighted by area and non-land fraction'
        #       
    #
    print('Creating dataset...')
    # Set up encoding and postprocess arrays into dataset.
    ds_out = xr.Dataset({avg_vars[0]: (['time'], avg_data[avg_vars[0]])},
                        coords={'time':(['time'],dates)})
    ds_out[avg_vars[0]].attrs = avg_data[avg_vars[0]].attrs
    t_units = 'days since 0000-01-01 00:00:00'
    t_cal = 'noleap'
    fill_val = 9.96921e+36
    wr_enc = {avg_vars[0]:{'_FillValue':fill_val},
              'time':{'units':t_units,'calendar':t_cal,
                      '_FillValue':fill_val}}
    for v in man_vars:
        ds_out[v] = man_data[v]
        ds_out[v].attrs = man_data[v].attrs
        wr_enc[v] = {'_FillValue':fill_val}
    for v in avg_vars[1:]:
        ds_out[v] = avg_data[v]
        ds_out[v].attrs = avg_data[v].attrs
        wr_enc[v] = {'_FillValue':fill_val}
    for v in glb_vars:
        ds_out[v] = glb_data[v]
        ds_out[v].attrs = glb_data[v].attrs
        wr_enc[v] = {'_FillValue':fill_val}
    #
    # Add global metadata.
    ds_out.attrs['case_name'] = case_name
    ds_out.attrs['pp_comment'] = 'Global averages calculated and concatenated by Jack Reeves Eyre (University of Arizona)'
    ds_out.attrs['pp_script'] = 'global_means_EAM.py'
    ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/oceanfluxes/src/master/'
    ds_out.attrs['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    #
    # Save data.
    print('Saving file...')
    ds_out.to_netcdf(path=save_dir+new_filename, mode='w',
                     encoding=wr_enc,unlimited_dims=['time'])
    #
    return

##########
# Function to create empty arrays.
def array_init(dates, varname):
    #
    da = xr.DataArray(np.zeros(len(dates)),
                      coords=[dates], dims=['time'],
                      name=varname)
    da.data[:] = np.nan
    return da

##########
# Function to calculate ocean average.
def oceanAverage(f):
    #
    av = (f*area*ocnfrac).sum()/(area*ocnfrac).sum()
    return av

##########
# Function to calculate global average.
def globalAverage(f):
    #
    av = (f*area).sum()/area.sum()
    return av

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
