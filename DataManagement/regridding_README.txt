################################################################################
# 0. Introduction ##############################################################
################################################################################

#### MPAS variables:
    timeMonthly_avg_latentHeatFlux
    timeMonthly_avg_sensibleHeatFlux
    timeMonthly_avg_windStressZonal
    timeMonthly_avg_windStressMeridional
    
#### CAM variables:
    LHFLX
    SHFLX
    TAUX
    TAUY

## MPAS ########################################################################
# 1. Climatology ###############################################################
################################################################################

#### Define CASENAME
export CASENAME=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil

#### Add _FillValue attributes (very slow - probably not important....).
cd /lcrc/group/acme/jeyre/archive/${CASENAME}/ocn/hist
for fl in `ls mpaso.hist.am.timeSeriesStatsMonthly.*` ; do
  ncatted -O -t -a _FillValue,timeMonthly_avg_latentHeatFlux,o,d,-9.99999979021476795361e+33 ${fl}
  ncatted -O -t -a _FillValue,timeMonthly_avg_sensibleHeatFlux,o,d,-9.99999979021476795361e+33 ${fl}
  ncatted -O -t -a _FillValue,timeMonthly_avg_windStressZonal,o,d,-9.99999979021476795361e+33 ${fl}
  ncatted -O -t -a _FillValue,timeMonthly_avg_windStressMeridional,o,d,-9.99999979021476795361e+33 ${fl}
done

#### Calculate climatologies.
ncclimo -m mpaso \
	-v timeMonthly_avg_latentHeatFlux,timeMonthly_avg_sensibleHeatFlux,timeMonthly_avg_windStressZonal,timeMonthly_avg_windStressMeridional  \
	-c ${CASENAME} \
	-s 2 -e 10 \
	--no_amwg_link \
	--clm_md=mth \
	-a sdd \
	-i /lcrc/group/acme/jeyre/archive/${CASENAME}/ocn/hist/ \
	-o /lcrc/group/acme/jeyre/archive/${CASENAME}/ocn/clim/


## MPAS ########################################################################
# 2. Differences ###############################################################
################################################################################

#### NCO uses following order:
# file_3 = file_1 - file_2
# ncbo --op_typ=- 1.nc 2.nc 3.nc

export CASENAME1=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME2=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export DIFFNAME=20191008.ocnSfcFlx_UA.minus.20191007.ocnSfcFlx_ctl

export FILEROOT=mpaso_ANN_000201_001012_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
		/lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_DJF_000201_001012_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_MAM_000203_001005_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_JJA_000206_001008_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_SON_000209_001011_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}

export CASENAME1=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME2=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export DIFFNAME=20191008.ocnSfcFlx_COARE.minus.20191007.ocnSfcFlx_ctl

export FILEROOT=mpaso_ANN_000201_001012_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_DJF_000201_001012_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_MAM_000203_001005_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_JJA_000206_001008_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_SON_000209_001011_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}

export CASENAME1=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME2=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export DIFFNAME=20191008.ocnSfcFlx_COARE.minus.20191008.ocnSfcFlx_UA

export FILEROOT=mpaso_ANN_000201_001012_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_DJF_000201_001012_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_MAM_000203_001005_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_JJA_000206_001008_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}
export FILEROOT=mpaso_SON_000209_001011_climo.nc
ncbo --op_typ=- /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME2}/ocn/clim/${FILEROOT} \
                /lcrc/group/acme/jeyre/archive/${CASENAME1}/ocn/clim/${DIFFNAME}_${FILEROOT}


## MPAS ########################################################################
# 3. Regridding ################################################################
################################################################################

############
# Mac OS X (old NCO) version:
    $NCARG_ROOT/bin/ESMF_RegridWeightGen -s /Users/jameseyre/Data/maps/SCRIP_files/mpas-ocean.oEC60to30v3.scrip.161222.nc -d /Users/jameseyre/Data/maps/SCRIP_files/180x360_GreenwichWest_SCRIP.20200203.nc -w /Users/jameseyre/Data/maps/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.bilin.20200210.nc --method bilinear --ignore_unmapped -p none --src_regional --dst_regional -l greatcirclec
    
    $NCARG_ROOT/bin/ESMF_RegridWeightGen -s /Users/jameseyre/Data/maps/SCRIP_files/mpas-ocean.oEC60to30v3.scrip.161222.nc -d /Users/jameseyre/Data/maps/SCRIP_files/180x360_GreenwichWest_SCRIP.20200203.nc -w /Users/jameseyre/Data/maps/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.conserve.20200210.nc --method conserve --ignore_unmapped -p none --src_regional --dst_regional -l greatcirclec

# (Run from data directories.)
for fl in `ls *.nc` ; do
    echo ${fl}
    ncks --map=/Users/jameseyre/Data/maps/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.bilin.20200210.nc ${fl} bilin_${fl}
    ncks --map=/Users/jameseyre/Data/maps/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.conserve.20200210.nc ${fl} conserve_${fl}
done

############
# Ubuntu (newer NCO) version:
#    ncremap -a conserve -g /home/jeyre/derived_data/maps/180x360_GreenwichWest_SCRIP.20200203.nc -s /home/jeyre/derived_data/maps/mpas-ocean.oEC60to30v3.scrip.161222.nc -M -m /home/jeyre/derived_data/maps/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.nc -O /lcrc/group/acme/jeyre/archive/${CASENAME}/ocn/clim/rgr/ -I /lcrc/group/acme/jeyre/archive/${CASENAME}/ocn/clim/

####
# Test if this gives the same results using:
export DATADIR=/home/jackre/Data/E3SM
export MAPDIR=/home/jackre/Data/maps
export CASENAME=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
ncremap -a conserve \
    -g ${MAPDIR}/SCRIP_files/180x360_GreenwichWest_SCRIP.20200203.nc \
    -s ${MAPDIR}/SCRIP_files/mpas-ocean.oEC60to30v3.scrip.161222.nc \
    -M \
    -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.conserve.test20200212.nc \
    -i ${DATADIR}/${CASENAME}/ocn/clim/mpaso_ANN_000201_001012_climo.nc \
    -o ${DATADIR}/${CASENAME}/ocn/clim/20191007.ocnSfcFlx_ctl.GMPAS_conserve_mpaso_ANN_000201_001012_climo_Ubuntu_NCOv4.9.1.nc
ncremap -a bilin \
    -g ${MAPDIR}/SCRIP_files/180x360_GreenwichWest_SCRIP.20200203.nc \
    -s ${MAPDIR}/SCRIP_files/mpas-ocean.oEC60to30v3.scrip.161222.nc \
    -M \
    -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.bilin.test20200212.nc \
    -i ${DATADIR}/${CASENAME}/ocn/clim/mpaso_ANN_000201_001012_climo.nc \
    -o ${DATADIR}/${CASENAME}/ocn/clim/20191007.ocnSfcFlx_ctl.GMPAS_bilin_mpaso_ANN_000201_001012_climo_Ubuntu_NCOv4.9.1.nc

####
# It does give the same results as the older (ncks) method, so do it this way for consistency with atmosphere methods.

declare -a CASENAME_ARRAY=("20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil" "20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil" "20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil")

for CASENAME in ${CASENAME_ARRAY[@]}
do
    ncremap -a conserve \
        -M \
        -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.conserve.20200212.nc \
        -I ${DATADIR}/${CASENAME}/ocn/clim/ \
        -O ${DATADIR}/${CASENAME}/ocn/clim/180x360_conserve
    ncremap -a bilin \
        -M \
        -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.bilin.20200212.nc \
        -I ${DATADIR}/${CASENAME}/ocn/clim/ \
        -O ${DATADIR}/${CASENAME}/ocn/clim/180x360_bilin
done

################################################################################
# CAM regridding ###############################################################
################################################################################


## CAM #########################################################################
# 1. Climatology ###############################################################
################################################################################

#### Define CASENAME
export CASENAME=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl

#### Calculate climatologies.
ncclimo -v lat,lon,area,LHFLX,SHFLX,TAUX,TAUY  \
	-c ${CASENAME} -m cam -h h0 \
	-s 2 -e 6 \
	--no_amwg_link \
	--clm_md=mth \
	-a sdd \
	-i /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASENAME}/atm/hist/ \
	-o /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASENAME}/atm/climo/ne30/


## CAM #########################################################################
# 2. Differences ###############################################################
################################################################################

#### NCO uses following order:
# file_3 = file_1 - file_2
# ncbo --op_typ=- 1.nc 2.nc 3.nc

declare -a FILEROOT_ARRAY=("_ANN_000201_000612_climo.nc" "_DJF_000201_000612_climo.nc" "_JJA_000206_000608_climo.nc" "_MAM_000203_000605_climo.nc" "_SON_000209_000611_climo.nc")

export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/
export CASENAME1=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASENAME2=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
export DIFFNAME=20190521.v1_UA.minus.20190521.v1_ctl

for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}${CASENAME1}/atm/climo/ne30/${CASENAME1}${FR} \
                    ${DATADIR}${CASENAME2}/atm/climo/ne30/${CASENAME2}${FR} \
	            ${DATADIR}${CASENAME1}/atm/climo/ne30/${DIFFNAME}${FR}
done


export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/
export CASENAME1=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
export CASENAME2=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
export DIFFNAME=20190621.v1_COARE.minus.20190521.v1_ctl

for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}${CASENAME1}/atm/climo/ne30/${CASENAME1}${FR} \
                    ${DATADIR}${CASENAME2}/atm/climo/ne30/${CASENAME2}${FR} \
	            ${DATADIR}${CASENAME1}/atm/climo/ne30/${DIFFNAME}${FR}
done


export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/
export CASENAME1=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
export CASENAME2=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export DIFFNAME=20190621.v1_COARE.minus.20190521.v1_UA

for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}${CASENAME1}/atm/climo/ne30/${CASENAME1}${FR} \
                    ${DATADIR}${CASENAME2}/atm/climo/ne30/${CASENAME2}${FR} \
	            ${DATADIR}${CASENAME1}/atm/climo/ne30/${DIFFNAME}${FR}
done

cd ${DATADIR}
tar -czvf ne30_ANN_SSN_0002_0006_climo.tar.gz ./20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30 ./20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30  ./20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30

## CAM #########################################################################
# 3. Regridding ################################################################
################################################################################

# SCRIP and mapping files taken from Charlie Zender's "library" on cori (/global/homes/z/zender/data/grids and /global/homes/z/zender/data/maps).
# Also, see useful background here:
# https://acme-climate.atlassian.net/wiki/spaces/ED/pages/754286611/Regridding+E3SM+Data+with+ncremap#RegriddingE3SMDatawithncremap-IntermediateRegriddingI:TreatmentofMissingData

export DATADIR=/home/jackre/Data/E3SM
export MAPDIR=/home/jackre/Data/maps
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl

ncremap -a conserve \
    -g ${MAPDIR}/SCRIP_files/180x360_GreenwichWest_SCRIP.20200203.nc \
    -s ${MAPDIR}/SCRIP_files/ne30np4_pentagons_dual_SCRIP.091226.nc \
    -M \
    -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_conserve.nc \
    -O ${DATADIR}/${CASENAME}/atm/climo/180x360/ \
    -I ${DATADIR}/${CASENAME}/atm/climo/ne30/
# Rename files:
for f in *climo.nc; do 
    mv -- "$f" "${f%.nc}.conserve.nc"
done


ncremap -a bilin \
    -g ${MAPDIR}/SCRIP_files/180x360_GreenwichWest_SCRIP.20200203.nc \
    -s ${MAPDIR}/SCRIP_files/ne30np4_pentagons_dual_SCRIP.091226.nc \
    -M \
    -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_bilin.nc \
    -O ${DATADIR}/${CASENAME}/atm/climo/180x360/ \
    -I ${DATADIR}/${CASENAME}/atm/climo/ne30/
# Rename files:
for f in *climo.nc; do 
    mv -- "$f" "${f%.nc}.bilin.nc"
done

# Repeat for other cases:
export CASENAME=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
# This time the mapping file already exists so don't need quite the same ncremap commands as above.
ncremap -a conserve \
    -M \
    -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_conserve.nc \
    -O ${DATADIR}/${CASENAME}/atm/climo/180x360/ \
    -I ${DATADIR}/${CASENAME}/atm/climo/ne30/
# Rename files:
for f in *climo.nc; do 
    mv -- "$f" "${f%.nc}.conserve.nc"
done
ncremap -a bilin \
    -M \
    -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_bilin.nc \
    -O ${DATADIR}/${CASENAME}/atm/climo/180x360/ \
    -I ${DATADIR}/${CASENAME}/atm/climo/ne30/
# Rename files:
for f in *climo.nc; do 
    mv -- "$f" "${f%.nc}.bilin.nc"
done

# Test if my map gives the same results as Charlie Zender's:
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl

ncremap -a conserve \
    -M \
    -m ${MAPDIR}/Zender_map_ne30np4_to_180x360wst_aave.20180301.nc \
    -o ${DATADIR}/${CASENAME}/atm/climo/180x360/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_ANN_000201_000612_climo.Zender_map_aave.nc \
    -i ${DATADIR}/${CASENAME}/atm/climo/ne30/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_ANN_000201_000612_climo.nc
ncremap -a bilin \
    -M \
    -m ${MAPDIR}/Zender_map_ne30np4_to_180x360wst_bilin.20180301.nc \
    -o ${DATADIR}/${CASENAME}/atm/climo/180x360/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_ANN_000201_000612_climo.Zender_map_bilin.nc \
    -i ${DATADIR}/${CASENAME}/atm/climo/ne30/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_ANN_000201_000612_climo.nc


################################################################################
# Time series regridding #######################################################
################################################################################

#####
##### F-cases on cori:
##### (02 March 2020)
export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive
export MAPDIR=/global/homes/j/jeyre/data/maps

declare -a CASENAME_ARRAY=("20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl" "20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl" "20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl")

for CASENAME in ${CASENAME_ARRAY[@]}
do
    ncremap -a conserve \
        -v LHFLX,SHFLX,TAUX,TAUY \
        -M \
        -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_conserve.nc \
        -I ${DATADIR}/${CASENAME}/atm/hist/ \
        -O ${DATADIR}/${CASENAME}/atm/hist/180x360_conserve
    ncremap -a bilin \
        -v LHFLX,SHFLX,TAUX,TAUY \
        -M \
        -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_bilin.nc \
        -I ${DATADIR}/${CASENAME}/atm/hist/ \
        -O ${DATADIR}/${CASENAME}/atm/hist/180x360_bilin
done

for CASENAME in ${CASENAME_ARRAY[@]}
do
    tar -zcf ${DATADIR}/${CASENAME}/atm/hist/${CASENAME}.cam.h0_180x360_bilin.tgz ${DATADIR}/${CASENAME}/atm/hist/180x360_bilin
    tar -zcf ${DATADIR}/${CASENAME}/atm/hist/${CASENAME}.cam.h0_180x360_conserve.tgz ${DATADIR}/${CASENAME}/atm/hist/180x360_conserve
done

scp jeyre@cori.nersc.gov:/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/hist/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl.cam.h0_180x360_bilin.tgz ./
scp jeyre@cori.nersc.gov:/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/hist/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl.cam.h0_180x360_conserve.tgz ./
scp jeyre@cori.nersc.gov:/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/hist/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl.cam.h0_180x360_bilin.tgz ./
scp jeyre@cori.nersc.gov:/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/hist/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl.cam.h0_180x360_conserve.tgz ./
scp jeyre@cori.nersc.gov:/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/hist/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl.cam.h0_180x360_bilin.tgz ./
scp jeyre@cori.nersc.gov:/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/hist/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl.cam.h0_180x360_conserve.tgz ./

#####
##### G-cases on Anvil
#####
export PATH="~zender/bin:${PATH}"
export LD_LIBRARY_PATH="~zender/lib:${LD_LIBRARY_PATH}"
export NCO_PATH_OVERRIDE=Yes
export DATADIR=/lcrc/group/acme/jeyre/archive
export MAPDIR=/home/jeyre/derived_data/maps
export FNROOT=mpaso.hist.am.timeSeriesStatsMonthly

declare -a CASENAME_ARRAY=("20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil" "20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil" "20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil")

for CASENAME in ${CASENAME_ARRAY[@]}
do
    ls ${DATADIR}/${CASENAME}/ocn/hist/${FNROOT}.[0-9]*.nc | \
        ncremap -a conserve \
        -v timeMonthly_avg_latentHeatFlux,timeMonthly_avg_sensibleHeatFlux,timeMonthly_avg_windStressZonal,timeMonthly_avg_windStressMeridional  \
        -M \
        -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.conserve.20200212.nc \
        -O ${DATADIR}/${CASENAME}/ocn/hist/180x360_conserve
    
    ls ${DATADIR}/${CASENAME}/ocn/hist/${FNROOT}.[0-9]*.nc | \
        ncremap -a bilin \
        -v timeMonthly_avg_latentHeatFlux,timeMonthly_avg_sensibleHeatFlux,timeMonthly_avg_windStressZonal,timeMonthly_avg_windStressMeridional \
        -M \
        -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.bilin.20200212.nc \
        -O ${DATADIR}/${CASENAME}/ocn/hist/180x360_bilin
	
done


tar -zcvf ${DATADIR}/mpaso.hist.am.timeSeriesStatsMonthly_regrid_20200306.tgz ${DATADIR}/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_bilin ${DATADIR}/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_conserve ${DATADIR}/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_bilin ${DATADIR}/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_conserve ${DATADIR}/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_bilin ${DATADIR}/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist/180x360_conserve

scp jeyre@blues.lcrc.anl.gov:/lcrc/group/acme/jeyre/archive/mpaso.hist.am.timeSeriesStatsMonthly_regrid_20200306.tgz ./

tar -xzf mpaso.hist.am.timeSeriesStatsMonthly_regrid_20200306.tgz




################################################################################
# CAM precip etc. ###############################################################
################################################################################
#
#### Environment for Cori.
export PATH="~zender/bin_cori:${PATH}"
export LD_LIBRARY_PATH="~zender/lib_cori:${LD_LIBRARY_PATH}"
#### Define CASENAME
export CASENAME=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
#----------------------------
#### Calculate climatologies.
ncclimo -v lat,lon,area,PRECC,PRECL,TREFHT,TS,QREFHT,U10  \
	-c ${CASENAME} -m cam -h h0 \
	-s 2 -e 6 \
	--no_amwg_link \
	--clm_md=mth \
	-a sdd \
	-i /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASENAME}/atm/hist/ \
	-o /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASENAME}/atm/climo/ne30/
#--------------------------
#### Calculate Differences.
#
#### NCO uses following order:
# file_3 = file_1 - file_2
# ncbo --op_typ=- 1.nc 2.nc 3.nc
#
declare -a FILEROOT_ARRAY=("_ANN_000201_000612_climo.nc" "_DJF_000201_000612_climo.nc" "_JJA_000206_000608_climo.nc" "_MAM_000203_000605_climo.nc" "_SON_000209_000611_climo.nc")
#
export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/
export CASENAME1=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASENAME2=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
export DIFFNAME=20190521.v1_UA.minus.20190521.v1_ctl
for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}${CASENAME1}/atm/climo/ne30/${CASENAME1}${FR} \
                    ${DATADIR}${CASENAME2}/atm/climo/ne30/${CASENAME2}${FR} \
	            ${DATADIR}${CASENAME1}/atm/climo/ne30/${DIFFNAME}${FR}
done
#
export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/
export CASENAME1=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
export CASENAME2=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
export DIFFNAME=20190621.v1_COARE.minus.20190521.v1_ctl
for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}${CASENAME1}/atm/climo/ne30/${CASENAME1}${FR} \
                    ${DATADIR}${CASENAME2}/atm/climo/ne30/${CASENAME2}${FR} \
	            ${DATADIR}${CASENAME1}/atm/climo/ne30/${DIFFNAME}${FR}
done
#
export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/
export CASENAME1=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
export CASENAME2=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export DIFFNAME=20190621.v1_COARE.minus.20190521.v1_UA
for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}${CASENAME1}/atm/climo/ne30/${CASENAME1}${FR} \
                    ${DATADIR}${CASENAME2}/atm/climo/ne30/${CASENAME2}${FR} \
	            ${DATADIR}${CASENAME1}/atm/climo/ne30/${DIFFNAME}${FR}
done
#
cd ${DATADIR}
tar -czvf ne30_ANN_SSN_0002_0006_climo_SurfaceMet.tar.gz ./20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30/*.nc ./20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30/*.nc  ./20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30/*.nc
#---------------
#### Regridding 
#
export DATADIR=/home/jackre/Data/E3SM
export MAPDIR=/home/jackre/Data/maps
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
ncremap -a conserve \
    -M \
    -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_conserve.nc \
    -O ${DATADIR}/${CASENAME}/atm/climo/180x360/SurfaceMet/ \
    -I ${DATADIR}/${CASENAME}/atm/climo/ne30/SurfaceMet/
# Rename files:
for f in *climo.nc; do 
    mv -- "$f" "${f%.nc}.conserve.nc"
done
ncremap -a bilin \
    -M \
    -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_bilin.nc \
    -O ${DATADIR}/${CASENAME}/atm/climo/180x360/SurfaceMet/ \
    -I ${DATADIR}/${CASENAME}/atm/climo/ne30/SurfaceMet/
# Rename files:
for f in *climo.nc; do 
    mv -- "$f" "${f%.nc}.bilin.nc"
done




################################################################################
# MPAS SSH etc. ################################################################
################################################################################
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# MOVE AND/OR RENAME PREVIOUS FILES BEFORE STARTING !!!!!
# ON ANVIL AND ON LAPTOP ---------------------------!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#### Environment for Blues.
export PATH="~zender/bin:${PATH}"
export LD_LIBRARY_PATH="~zender/lib:${LD_LIBRARY_PATH}"
export NCO_PATH_OVERRIDE=Yes
export DATADIR=/lcrc/group/acme/jeyre/archive
# Casenames:
export CASENAME=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil
#----------------------------
#### Calculate climatologies
#    Just for one year - to compare SSH in year 10 between runs.
ncclimo -m mpaso \
	-v timeMonthly_avg_pressureAdjustedSSH  \
	-c ${CASENAME} \
	-s 10 -e 10 \
	--no_amwg_link \
	--clm_md=mth \
	-a sdd \
	-i ${DATADIR}/${CASENAME}/ocn/hist/ \
	-o ${DATADIR}/${CASENAME}/ocn/clim/SSH/
#--------------------------
#### Calculate Differences.
#
#### NCO uses following order:
# file_3 = file_1 - file_2
# ncbo --op_typ=- 1.nc 2.nc 3.nc
#
declare -a FILEROOT_ARRAY=("mpaso_ANN_001001_001012_climo.nc" "mpaso_DJF_001001_001012_climo.nc" "mpaso_JJA_001006_001008_climo.nc" "mpaso_MAM_001003_001005_climo.nc" "mpaso_SON_001009_001011_climo.nc")
#
export CASENAME1=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME2=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export DIFFNAME=20191008.ocnSfcFlx_UA.minus.20191007.ocnSfcFlx_ctl
for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}/${CASENAME1}/ocn/clim/SSH/${FR} \
                    ${DATADIR}/${CASENAME2}/ocn/clim/SSH/${FR} \
	            ${DATADIR}/${CASENAME1}/ocn/clim/SSH/${DIFFNAME}_${FR}
done
#
export CASENAME1=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME2=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export DIFFNAME=20191008.ocnSfcFlx_COARE.minus.20191007.ocnSfcFlx_ctl
for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}/${CASENAME1}/ocn/clim/SSH/${FR} \
                    ${DATADIR}/${CASENAME2}/ocn/clim/SSH/${FR} \
	            ${DATADIR}/${CASENAME1}/ocn/clim/SSH/${DIFFNAME}_${FR}
done
#
export CASENAME1=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME2=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export DIFFNAME=20191008.ocnSfcFlx_COARE.minus.20191008.ocnSfcFlx_UA
for FR in ${FILEROOT_ARRAY[@]}
do
    ncbo --op_typ=- ${DATADIR}/${CASENAME1}/ocn/clim/SSH/${FR} \
                    ${DATADIR}/${CASENAME2}/ocn/clim/SSH/${FR} \
	            ${DATADIR}/${CASENAME1}/ocn/clim/SSH/${DIFFNAME}_${FR}
done
#
cd ${DATADIR}
tar -czvf mpaso_ANN_SSN_0010_0010_climo_SSH.tar.gz ./20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/SSH/ ./20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/SSH/ ./20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/clim/SSH/
#
scp jeyre@blues.lcrc.anl.gov:/lcrc/group/acme/jeyre/archive/mpaso_ANN_SSN_0010_0010_climo_SSH.tar.gz ./
#
tar -xzvf mpaso_ANN_SSN_0010_0010_climo_SSH.tar.gz
#---------------
#### Regridding 
#
export DATADIR=/home/jackre/Data/E3SM
export MAPDIR=/home/jackre/Data/maps
#
export CASENAME=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil
ncremap -a conserve \
    -M \
    -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.conserve.20200212.nc \
    -I ${DATADIR}/${CASENAME}/ocn/clim/SSH/ \
    -O ${DATADIR}/${CASENAME}/ocn/clim/180x360_conserve/SSH/
#
ncremap -a bilin \
    -M \
    -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.bilin.20200212.nc \
    -I ${DATADIR}/${CASENAME}/ocn/clim/SSH/ \
    -O ${DATADIR}/${CASENAME}/ocn/clim/180x360_bilin/SSH/

################################################################################
# Joining MPAS files for global total evaporation. #############################
################################################################################

cd /lcrc/group/acme/jeyre/archive/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist
ncrcat -v timeMonthly_avg_areaCellGlobal,timeMonthly_avg_sumGlobalStats_evaporationFluxSum,timeMonthly_avg_sumGlobalStats_rainFluxSum,timeMonthly_avg_sumGlobalStats_snowFluxSum,timeMonthly_avg_sumGlobalStats_seaIceFreshWaterFluxSum,timeMonthly_avg_sumGlobalStats_icebergFreshWaterFluxSum,timeMonthly_avg_sumGlobalStats_riverRunoffFluxSum,timeMonthly_avg_sumGlobalStats_iceRunoffFluxSum,timeMonthly_avg_sumGlobalStats_landIceFreshwaterFluxSum mpaso.hist.am.timeSeriesStatsMonthly.00[01]?-??-01.nc /home/jeyre/derived_data/OceanFluxes/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil_mpaso.hist.am.timeSeriesStatsMonthly_GlobalEvaporationFluxSum.nc

cd /lcrc/group/acme/jeyre/archive/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist
ncrcat -v timeMonthly_avg_areaCellGlobal,timeMonthly_avg_sumGlobalStats_evaporationFluxSum,timeMonthly_avg_sumGlobalStats_rainFluxSum,timeMonthly_avg_sumGlobalStats_snowFluxSum,timeMonthly_avg_sumGlobalStats_seaIceFreshWaterFluxSum,timeMonthly_avg_sumGlobalStats_icebergFreshWaterFluxSum,timeMonthly_avg_sumGlobalStats_riverRunoffFluxSum,timeMonthly_avg_sumGlobalStats_iceRunoffFluxSum,timeMonthly_avg_sumGlobalStats_landIceFreshwaterFluxSum mpaso.hist.am.timeSeriesStatsMonthly.00[01]?-??-01.nc /home/jeyre/derived_data/OceanFluxes/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil_mpaso.hist.am.timeSeriesStatsMonthly_GlobalEvaporationFluxSum.nc

cd /lcrc/group/acme/jeyre/archive/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/ocn/hist
ncrcat -v timeMonthly_avg_areaCellGlobal,timeMonthly_avg_sumGlobalStats_evaporationFluxSum,timeMonthly_avg_sumGlobalStats_rainFluxSum,timeMonthly_avg_sumGlobalStats_snowFluxSum,timeMonthly_avg_sumGlobalStats_seaIceFreshWaterFluxSum,timeMonthly_avg_sumGlobalStats_icebergFreshWaterFluxSum,timeMonthly_avg_sumGlobalStats_riverRunoffFluxSum,timeMonthly_avg_sumGlobalStats_iceRunoffFluxSum,timeMonthly_avg_sumGlobalStats_landIceFreshwaterFluxSum mpaso.hist.am.timeSeriesStatsMonthly.00[01]?-??-01.nc /home/jeyre/derived_data/OceanFluxes/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil_mpaso.hist.am.timeSeriesStatsMonthly_GlobalEvaporationFluxSum.nc


################################################################################
# Regridding GPCP for bias calculation. ########################################
################################################################################

ncremap -a bilin -d /home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_01_000201_000601_climo.bilin.nc -i /home/jackre/Data/satellite/GPCP/GPCP.v2.3.precip.mon.ltm.nc -o /home/jackre/Data/satellite/GPCP/GPCP.v2.3.precip.mon.ltm_180x360_GreenwichWest_bilin.nc -m /home/jackre/Data/maps/map_GPCP_to_180x360_GreenwichWest_bilin.nc

ncremap -a conserve -d /home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/180x360/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_01_000201_000601_climo.bilin.nc -i /home/jackre/Data/satellite/GPCP/GPCP.v2.3.precip.mon.ltm.nc -o /home/jackre/Data/satellite/GPCP/GPCP.v2.3.precip.mon.ltm_180x360_GreenwichWest_conserve.nc -m /home/jackre/Data/maps/map_GPCP_to_180x360_GreenwichWest_conserve.nc


ncremap -a bilin -i /home/jackre/Data/satellite/GPCP/GPCP.v2.3.precip.mon.mean.nc -o /home/jackre/Data/satellite/GPCP/GPCP.v2.3.precip.mon.mean_180x360_GreenwichWest_bilin.nc -m /home/jackre/Data/maps/map_GPCP_to_180x360_GreenwichWest_bilin.nc

ncremap -a conserve -i /home/jackre/Data/satellite/GPCP/GPCP.v2.3.precip.mon.mean.nc -o /home/jackre/Data/satellite/GPCP/GPCP.v2.3.precip.mon.mean_180x360_GreenwichWest_conserve.nc -m /home/jackre/Data/maps/map_GPCP_to_180x360_GreenwichWest_conserve.nc





################################################################################
# CAM U, T200 and T850 #########################################################
################################################################################
#
#### Environment for Cori.
export PATH="~zender/bin_cori:${PATH}"
export LD_LIBRARY_PATH="~zender/lib_cori:${LD_LIBRARY_PATH}"
export NCO_PATH_OVERRIDE=Yes
#### Define CASENAME
export CASE=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
export CASE=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASE=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
#----------------------------

# Create file with output pressure levels.
ncap2 -O -v -s 'defdim("plev",2);plev[$plev]={85000,20000};' /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/vrt_prs_850_200.nc

# Regridding and climatology of standard output variables.
ncclimo \
-i /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/hist/ \
-c ${CASE} \
-v T200,U,T \
-O /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/climo/180x360_bilin/ \
-o /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/climo/ne30/ \
-r /global/homes/j/jeyre/data/maps/map_ne30np4_to_180x360_GreenwichWest_bilin.nc \
-s 2 -e 6 --no_amwg_link

# Do vertical interpolation.
ncremap \
-v T \
-I /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/hist/ \
-O /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/hist/plev/ \
--vrt_fl=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/vrt_prs_850_200.nc \
--vrt_xtr=mss_val --vrt_ntp=log

# Do horizontal interpolation/regridding and climatology.
ncclimo \
-i /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/hist/plev/ \
-c ${CASE} \
-O /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/climo/180x360_bilin/plev/ \
-o /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/climo/ne30/plev/ \
-r /global/homes/j/jeyre/data/maps/map_ne30np4_to_180x360_GreenwichWest_bilin.nc \
-s 2 -e 6 --no_amwg_link

cp /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/climo/180x360_bilin/plev/${CASE}_[ADJ]* ~/data/OceanFluxes/e3sm_diags/${CASE}/atm/e3sm_diags_data/plev/

cp /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASE}/atm/climo/180x360_bilin/${CASE}_[ADJ]* ~/data/OceanFluxes/e3sm_diags/${CASE}/atm/e3sm_diags_data/

tar -zcvf ~/data/OceanFluxes/e3sm_diags.tgz ~/data/OceanFluxes/e3sm_diags/

# Follow up work on local machine:
ncap2 -O -v -s 'defdim("plev",19);plev[$plev]={100000,95000,90000,85000,80000,75000,70000,65000,60000,55000,50000,45000,40000,35000,30000,25000,20000,15000,10000};' ~/Data/maps/vertical_grids/vrt_prs_1000_100_by50.nc

ncremap \
-v U \
-I ~/Data/E3SM/${CASE}/atm/e3sm_diags_data/ \
-O ~/Data/E3SM/${CASE}/atm/e3sm_diags_data/U_plev/ \
--vrt_fl=/home/jackre/Data/maps/vertical_grids/vrt_prs_1000_100_by50.nc \
--vrt_xtr=mss_val --vrt_ntp=log


################################################################################
# TOA variables. ###############################################################
################################################################################
#
#### Environment for Cori.
export PATH="~zender/bin_cori:${PATH}"
export LD_LIBRARY_PATH="~zender/lib_cori:${LD_LIBRARY_PATH}"
export NCO_PATH_OVERRIDE=Yes
#### Define CASENAME
export CASENAME=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
#----------------------------
#### Calculate climatologies.
ncclimo -v lat,lon,area,FLNT,FLNTC,FLUT,FLUTC,FSNT,FSNTC,FSNTOA,FSNTOAC,FSUTOA,FSUTOAC,LWCF,SWCF,SOLIN  \
	-c ${CASENAME} -m cam -h h0 \
	-s 2 -e 6 \
	--no_amwg_link \
	--clm_md=mth \
	-a sdd \
	-i /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASENAME}/atm/hist/ \
	-o /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/${CASENAME}/atm/climo/ne30/TOA/
	
#--------------------------
cd /global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive/
tar -czvf ne30_0002_0006_climo_TOA.tar.gz ./20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30/TOA/*.nc ./20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30/TOA/*.nc  ./20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/climo/ne30/TOA/*.nc

#---------------
#### Regridding 
#
export DATADIR=/home/jackre/Data/E3SM
export MAPDIR=/home/jackre/Data/maps
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
export CASENAME=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
cd ${DATADIR}/${CASENAME}/atm/climo/180x360/TOA/
ncremap -a conserve \
    -M \
    -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_conserve.nc \
    -O ${DATADIR}/${CASENAME}/atm/climo/180x360/TOA/ \
    -I ${DATADIR}/${CASENAME}/atm/climo/ne30/TOA/
# Rename files:
for f in *climo.nc; do 
    mv -- "$f" "${f%.nc}.conserve.nc"
done
#
ncremap -a bilin \
    -M \
    -m ${MAPDIR}/map_ne30np4_to_180x360_GreenwichWest_bilin.nc \
    -O ${DATADIR}/${CASENAME}/atm/climo/180x360/TOA/ \
    -I ${DATADIR}/${CASENAME}/atm/climo/ne30/TOA/
# Rename files:
for f in *climo.nc; do 
    mv -- "$f" "${f%.nc}.bilin.nc"
done


################################################################################
# MPAS SST. ####################################################################
################################################################################
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# MOVE AND/OR RENAME PREVIOUS FILES BEFORE STARTING !!!!!
# ON ANVIL AND ON LAPTOP ---------------------------!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#### Environment for Blues.
export PATH="~ac.zender/bin:${PATH}"
export LD_LIBRARY_PATH="~ac.zender/lib:${LD_LIBRARY_PATH}"
export NCO_PATH_OVERRIDE=Yes
export DATADIR=/lcrc/group/acme/ac.jeyre/archive
# Casenames:
export CASENAME=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil
#----------------------------
#### Calculate climatologies
#    Just for one year - to compare SSH in year 10 between runs.
ncclimo -m mpaso \
	-v timeMonthly_avg_activeTracers_temperature  \
	-c ${CASENAME} \
	-s 2 -e 10 \
	--no_amwg_link \
	--clm_md=mth \
	-a sdd \
	-i ${DATADIR}/${CASENAME}/ocn/hist/ \
	-o ${DATADIR}/${CASENAME}/ocn/clim/SST/

#### Regridding on laptop
export DATADIR=/home/jackre/Data/E3SM
export MAPDIR=/home/jackre/Data/maps
#
export CASENAME=20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil
export CASENAME=20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil
#
# Might need to use:
ncpdq -a Time,nVertLevels,nCells mpaso_ANN_000201_001012_climo.nc mpaso_ANN_000201_001012_climo_TZXY.nc
#
mkdir ${DATADIR}/${CASENAME}/ocn/clim/180x360_conserve/SST
ncremap -a conserve \
    -M \
    -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.conserve.20200212.nc \
    -I ${DATADIR}/${CASENAME}/ocn/clim/SST/ \
    -O ${DATADIR}/${CASENAME}/ocn/clim/180x360_conserve/SST/
#
mkdir ${DATADIR}/${CASENAME}/ocn/clim/180x360_bilin/SST
ncremap -a bilin \
    -M \
    -m ${MAPDIR}/map_mpas-ocean.oEC60to30v3_to_180x360_GreenwichWest.bilin.20200212.nc \
    -I ${DATADIR}/${CASENAME}/ocn/clim/SST/ \
    -O ${DATADIR}/${CASENAME}/ocn/clim/180x360_bilin/SST/

# Needed for control run.
ncatted -O -t -a _FillValue,timeMonthly_avg_activeTracers_temperature,o,d,-9.99999979021476795361e+33 mpaso.hist.am.timeSeriesStatsMonthly.0004-12-01.nc


################################################################################
# JRA wind speed. ##############################################################
################################################################################
# No SCRIP file -- have to infer using ncks
ncks --rgr infer \
     --rgr scrip=~/Data/maps/SCRIP_files/JRA55_TL319_SCRIP.20201110.nc \
     ~/Data/reanalysis/JRA55/JRA.v1.3.Umag_10.TL319.1959-1967.nc \
     ~/Data/maps/dummy_files/foo.nc

ncremap -a conserve \
    -g ~/Data/maps/SCRIP_files/180x360_GreenwichWest_SCRIP.20200203.nc \
    -s ~/Data/maps/SCRIP_files/JRA55_TL319_SCRIP.20201110.nc \
    -M \
    -m ~/Data/maps/map_JRA55_TL319_to_180x360_GreenwichWest_conserve.nc \
    -o ~/Data/reanalysis/JRA55/JRA.v1.3.Umag_10.TL319.1959-1967_180x360_conserve.nc \
    -i ~/Data/reanalysis/JRA55/JRA.v1.3.Umag_10.TL319.1959-1967.nc
    
ncremap -a bilin \
    -g ~/Data/maps/SCRIP_files/180x360_GreenwichWest_SCRIP.20200203.nc \
    -s ~/Data/maps/SCRIP_files/JRA55_TL319_SCRIP.20201110.nc \
    -M \
    -m ~/Data/maps/map_JRA55_TL319_to_180x360_GreenwichWest_bilin.nc \
    -o ~/Data/reanalysis/JRA55/JRA.v1.3.Umag_10.TL319.1959-1967_180x360_bilin.nc \
    -i ~/Data/reanalysis/JRA55/JRA.v1.3.Umag_10.TL319.1959-1967.nc 
