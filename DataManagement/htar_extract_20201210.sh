#!/bin/bash -l
#SBATCH -q xfer
#SBATCH -t 12:00:00
#SBATCH -J flux_extract_archive

# -------------------------------
# Submit job with
#     module load esslurm
#     sbatch <job_script>
# -------------------------------
#
export DATADIR=/global/cscratch1/sd/jeyre/acme_scratch/cori-knl/archive
export HPSSDIR=/home/j/jeyre/ocn_surface_flux
#
export CASENAME=20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl
cd ${DATADIR}/${CASENAME}/atm/hist/
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h0.0001-0006.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0002.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0003.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0004.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0005.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0006.tar
#
export CASENAME=20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl
cd ${DATADIR}/${CASENAME}/atm/hist/
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h0.0001-0006.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0002.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0003.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0004.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0005.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0006.tar
#
export CASENAME=20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl
cd ${DATADIR}/${CASENAME}/atm/hist/
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h0.0001-0006.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0002.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0003.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0004.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0005.tar
htar -xv -f ${HPSSDIR}/${CASENAME}/${CASENAME}.cam.h1.0006.tar
#
#
# -------------------------------

