"""
Plot global maps of differences between algorithms for model
forcing variables (SST, wind speed):

Call as:
    $ python ForcingDiffComparison_GlobalMap.py var season interp
with command line arguments:
    var -- the variable: TS, U10
    season -- the averaging season: ANN, DJF, MAM, JJA, SON 
    interp -- the interpolation method: conserve, bilin
All arguments are required and the script fails without them.

Layout of plot panels:
    ----------------------------------------------
    |  1. model: UA-ctl    |  2. forcing-ctl     |
    ----------------------------------------------
    |  3. model: COARE-ctl |  4. forcing-UA      |
    ----------------------------------------------
    |  5. model: COARE-UA  |  6. forcing-COARE   |
    ----------------------------------------------

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
#
from SignificanceBenchmark_GlobalMap import *
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 4:
        sys.exit("Incorrect number of command line arguments (3 required)")
    else:
        var_names = parse_var(argv[1])
        season = check_season(argv[2])
        interp = check_interp(argv[3])
    #
    # Load data.
    print('-----\nReading data...\n-----')
    plot_data,plot_names = load_all_data(var_names, season, interp)
    #
    # Plot maps.
    print('-----\nPlotting... \n-----')
    plot_diffs(plot_data, plot_names,
               var_names, season, interp)
    #
    return()

################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'TS':
        var_name_dict['short'] = 'TS'
        var_name_dict['forcing'] = 'TS'
        var_name_dict['model'] = 'timeMonthly_avg_activeTracers_temperature'
    elif v == 'U10':
        var_name_dict['short'] = 'U10'
        var_name_dict['model'] = 'U10'
        var_name_dict['forcing'] = 'Umag_10'
    else:
        sys.exit('Variable name not valid - must be one of (TS, U10).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'MAM', 'JJA', 'SON']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['conserve', 'bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (conserve, bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_all_data(v,s,i):
    """ Loads top-of-atmosphere radiation data.
    Gives both differences between runs and biases vs. observations. 
    """
    #
    # Define file names and data types:
    file_dict = {'TS':{'ctl':glob.glob('/home/jackre/Data/E3SM/20191007.ocnSfcFlx_ctl.GMPAS-JRA.TL319_oEC60to30v3.anvil/' +
                                       'ocn/clim/180x360_' + i + '/SST/mpaso_' + s + '_*_climo_TZXY.nc')[0],
                       'UA':glob.glob('/home/jackre/Data/E3SM/20191008.ocnSfcFlx_UA.GMPAS-JRA.TL319_oEC60to30v3.anvil/' +
                                      'ocn/clim/180x360_' + i + '/SST/mpaso_' + s + '_*_climo_TZXY.nc')[0],
                       'COARE':glob.glob('/home/jackre/Data/E3SM/20191008.ocnSfcFlx_COARE.GMPAS-JRA.TL319_oEC60to30v3.anvil/' +
                                         'ocn/clim/180x360_' + i + '/SST/mpaso_' + s + '_*_climo_TZXY.nc')[0],
                       'forcing':glob.glob('/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/' +
                                           'atm/climo/180x360/SurfaceMet/' +
                                           '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_' + s + '_*_climo.' + i + '.nc')[0]},
                 'U10':{'ctl':glob.glob('/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/' +
                                        'atm/climo/180x360/SurfaceMet/' +
                                        '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_' + s + '_*_climo.' + i + '.nc')[0],
                        'UA':glob.glob('/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/' +
                                       'atm/climo/180x360/SurfaceMet/' +
                                       '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl_' + s + '_*_climo.' + i + '.nc')[0],
                        'COARE':glob.glob('/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/' +
                                          'atm/climo/180x360/SurfaceMet/' +
                                          '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl_' + s + '_*_climo.' + i + '.nc')[0],
                        'forcing':'/home/jackre/Data/reanalysis/JRA55/JRA.v1.3.Umag_10.TL319.1959-1967_180x360_' + i + '.nc'}
    }
    #
    # Add all data to array list - as final quantities.
    array_list = []
    if v['short'] == 'TS':
        for i_fn,fn in enumerate(['ctl','UA','COARE']):
            ds = xr.open_dataset(file_dict[v['short']][fn], decode_times=False)
            array_list.append(ds[v['model']][0,0,:,:])
        ds = xr.open_dataset(file_dict[v['short']]['forcing'], decode_times=False)
        array_list.append(ds[v['forcing']][0,:,:] - 273.15)
    elif v['short'] == 'U10':
        for i_fn,fn in enumerate(['ctl','UA','COARE']):
            ds = xr.open_dataset(file_dict[v['short']][fn], decode_times=False)
            array_list.append(ds[v['model']][0,:,:])
        ds = xr.open_dataset(file_dict[v['short']]['forcing']).rename({'latitude':'lat', 'longitude':'lon'})
        ds = ds.assign_coords(lon=ds.lon,lat=ds.lat)
        array_list.append(seasonal_mean_from_LTA(ds[v['forcing']].groupby('time.month').mean('time'),s))
    else:
        sys.exit('Variable name not valid - load_TOA_data takes one of (TS,U10).')
    #
    # Define plot titles.
    plot_names = ['UA - control',
                  'control - forcing',
                  'COARE - control',
                  'UA - forcing',
                  'COARE - UA',
                  'COARE - forcing']
    plus_order = [1,0,2,1,2,2]
    minus_order = [0,3,0,3,1,3]
    #
    # Calculate specified differences, and mask.
    data_dict = {}
    ds_mask = xr.open_dataset(
        '/home/jackre/Data/maps/masks/ocean_mask_180x360.nc')
    mask_var = ds_mask.ocean_mask
    for i_diff,diff in enumerate(plot_names):
        data_dict[diff] = (array_list[plus_order[i_diff]] -
                           array_list[minus_order[i_diff]])
        data_dict[diff].data = mask_var.data*data_dict[diff].data 
    #
    # Print some statistics.
    print('------------------------------' + v['short'] +
          '------------------------------')
    for b in plot_names:
        print('----------' + b + '----------')
        print('----------' + 'Mean bias' + '----------')
        print("{:+.4f}".format(global_mean(data_dict[b]).data))
        print('----------' + 'RMSD' + '----------')
        print("{:+.4f}".format(global_RMS(data_dict[b]).data))
    #
    # Print in LaTeX form.
    if 1:
        print('\\begin{table}[ht]')
        print('\\caption{INSERT CAPTION HERE}')
        print('\\centering')
        print('\\begin{tabular}{p{6cm} c c c c c c}')
        print(' &  & Bias &  &  & RMSD &  \\\\')
        print('\\hline')
        print(' & control & UA & COARE & control & UA & COARE \\\\')
        print('\\hline')
        print(v['short'] + '$\ (W~m^{-2})\ $' + ' & ' +
              "{:+.2f}".format(global_mean(data_dict[plot_names[1]]).data) +
              ' & ' +
              "{:+.2f}".format(global_mean(data_dict[plot_names[3]]).data) +
              ' & ' +
              "{:+.2f}".format(global_mean(data_dict[plot_names[5]]).data) +
              ' & ' +
              "{:+.2f}".format(global_RMS(data_dict[plot_names[1]]).data) +
              ' & ' +
              "{:+.2f}".format(global_RMS(data_dict[plot_names[3]]).data) +
              ' & ' +
              "{:+.2f}".format(global_RMS(data_dict[plot_names[5]]).data) +
              ' & ')
        print('\\end{tabular}')
        print('\\label{obs_bias_table}')
        print('\\end{table}')
    #
    # Return data.
    return(data_dict, plot_names)


def seasonal_mean_from_LTA(LTA,ssn):
    """Calculates weighted seasonal (or annual) mean from monthly long 
    term average data (i.e, an array with size (12 x nlat x nlon)).
    """
    #
    # Define days per month.
    dpm = np.array([31,28,31,30,31,30,31,31,30,31,30,31])
    #
    # Get months for the season - ZERO INDEXED!.
    if ssn == 'ANN':
        s_m = np.arange(0,12,dtype=np.int)
    elif ssn == 'DJF':
        s_m = np.array([0,1,11],dtype=np.int)
    elif ssn == 'MAM':
        s_m = np.array([2,3,4],dtype=np.int)
    elif ssn == 'JJA':
        s_m = np.array([5,6,7],dtype=np.int)
    elif ssn == 'SON':
        s_m = np.array([8,9,10],dtype=np.int)
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, MAM, JJA, SON).')
    #
    # Calculate days in this season.
    dps = np.sum(dpm[s_m])
    #
    # Get first month and weight it.
    ann_LTA = LTA[s_m[0],:,:]*dpm[s_m[0]]
    #
    # Loop over other months.
    for i in range(1,len(s_m)):
        ann_LTA = ann_LTA + LTA[s_m[i],:,:]*dpm[s_m[i]]
    #
    # Divide by days in season.
    ann_LTA = ann_LTA/dps
    #
    return ann_LTA

################################################################################
# Functions to calculate stats.
################################################################################

# Calculate the global mean value of a global map.
# Assumes regular grid and applies cosine weighting.
def global_mean(diff_map):
    #
    # Create map of cosine(latitude) weights.
    cos_map = xr.zeros_like(diff_map)
    cos_map.data = np.broadcast_to(np.expand_dims(
        np.cos(diff_map.lat*np.pi/180.0),axis=1),
                                   diff_map.shape)
    #
    # Calculate mean.
    diff_mean = (diff_map*cos_map).sum()/(cos_map.sum())
    #
    return diff_mean

# Calculate the global RMS value of a global map.
# Assumes regular grid and applies cosine weighting.
def global_RMS(diff_map):
    #
    # Create map of cosine(latitude) weights.
    cos_map = xr.zeros_like(diff_map)
    cos_map.data = np.broadcast_to(np.expand_dims(
        np.cos(diff_map.lat*np.pi/180.0),axis=1),
                                   diff_map.shape)
    #
    # Calculate RMS.
    diff_RMS = np.sqrt((diff_map*diff_map*cos_map).sum()/(cos_map.sum()))
    #
    return diff_RMS

################################################################################
# Functions to plot maps.
################################################################################


def plot_diffs(data_dict, name_list, var_name_dict, ssn, interp):
    """Plot precip maps.
    """
    if (var_name_dict['short'] not in ['TS','U10']):
        sys.exit('plot_toa_diffs only works for TS, U10')
    #
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['short'], ssn)
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 2),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axgr):
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[name_list[i]].coords['lat'],
                                 data_dict[name_list[i]].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(name_list[i],loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm, data_dict[name_list[i]].data,
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        #
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    #
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p)
    cb.set_label_text(plot_details['units'])
    #
    # Add over all details.
    fig.suptitle(plot_details['main_title'])
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + var_name_dict['short'] +
                '_' + ssn +
                '_' + interp +
                '.png',
                format='png')

    return()

def plot_info(v,s):
    """Give some details for plotting, based on variable name.
    Returns a dictionary with units, min and max for color scale.
    """
    plot_info_dict = {}
    # Season-specific bits.
    if s == 'ANN':
        s_string = 'annual mean '
    else:
        s_string = s + ' mean '
    # General details.
    if v == 'TS':
        plot_info_dict['units'] = r'$^{o}\! C$'
        plot_info_dict['main_title'] = s_string + \
                                       'SST'
        plot_info_dict['min'] = -1.0      
        plot_info_dict['max'] = 1.0       
        plot_info_dict['color_step'] = 0.25
    elif v == 'U10':
        plot_info_dict['units'] = r'$m\! s^{-1}$'
        plot_info_dict['main_title'] = s_string + \
                                       '10m wind speed'
        plot_info_dict['min'] = -2.0      
        plot_info_dict['max'] = 2.0       
        plot_info_dict['color_step'] = 0.5
        
    else:
        sys.exit('Variable name not valid - must be one of (TS, U10).')
    return(plot_info_dict)

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
