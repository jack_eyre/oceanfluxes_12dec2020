"""
Plot global maps of differences between algorithms for atmo runs, for 
replication of E3SM-diags maps:

Call as:
    $ python AtmoDiffComparison_GlobalMap_E3SMdiags.py var season interp
with command line arguments:
    var -- the variable: T200_model, T200, T850
    season -- the averaging season: ANN, DJF, JJA 
    interp -- the interpolation method: bilin
All arguments are required and the script fails without them.

Layout of plot panels:
    -----------------------
    |  1. atmo: UA-ctl    |
    -----------------------
    |  2. atmo: COARE-ctl |
    -----------------------
    |  3. atmo: COARE-UA  |
    -----------------------

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 4:
        sys.exit("Incorrect number of command line arguments (3 required)")
    else:
        var_names = parse_var(argv[1])
        season = check_season(argv[2])
        interp = check_interp(argv[3])
    #
    # Load data.
    print('-----\nReading data...\n-----')
    plot_data,plot_names = load_data(var_names, season, interp)
    #
    # Plot maps.
    print('-----\nPlotting... \n-----')
    plot_diff_maps(plot_data, plot_names,
                   var_names, season, interp)
    #
    return()


################################################################################
# Functions to help parse command line arguments.
################################################################################

def parse_var(v):
    """Check for valid variable name 
    and return dictionary with ocean equivalent variable name.
    """
    var_name_dict = {}
    if v == 'T200_model':
        var_name_dict['atmo'] = 'T200_model'
        var_name_dict['filevar'] = 'T200'
    elif v == 'T200':
        var_name_dict['atmo'] = 'T200'
        var_name_dict['filevar'] = 'T'
        var_name_dict['level'] = 20000.0
    elif v == 'T850':
        var_name_dict['atmo'] = 'T850'
        var_name_dict['filevar'] = 'T'
        var_name_dict['level'] = 85000.0
    else:
        sys.exit('Variable name not valid - must be one of (T200_model,T200,T850).')
    return(var_name_dict)

def check_season(s):
    """Check for valid season name.
    """
    if s in ['ANN', 'DJF', 'JJA']:
        season = s
    else:
        sys.exit('Season name not valid - must be one of (ANN, DJF, JJA).')
    return(season)
      
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_data(v,s,i):
    """Loads data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    #
    # Define file names and data types:
    if v['atmo'] in ['T200_model']:
        ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/'    
        UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/'      
        COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/'
    elif v['atmo'] in ['T200', 'T850']:
        ctl_a_dir = '/home/jackre/Data/E3SM/20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/plev/'    
        UA_a_dir = '/home/jackre/Data/E3SM/20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/plev/'      
        COARE_a_dir = '/home/jackre/Data/E3SM/20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl/atm/e3sm_diags_data/plev/'
    #
    filenames = [glob.glob(ctl_a_dir +
                           '20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.nc'),
                 glob.glob(UA_a_dir +
                           '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.nc'),
                 glob.glob(COARE_a_dir +
                           '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl_' +
                           s + '_*_climo.nc')]
    #
    # Define plot titles.
    clim_names = ['control',
                  'UA',
                  'COARE']
    #
    # Read data.
    clim_dict = {}
    for i_fn,fn in enumerate(filenames):
        ds = xr.open_dataset(fn[0], decode_times=False)
        if v['atmo'] == 'T200_model':
            clim_dict[clim_names[i_fn]] = ds[v['filevar']][0,:,:]
        elif v['atmo'] in ['T200', 'T850']:
            clim_dict[clim_names[i_fn]] = ds[v['filevar']][0,:,:,:].\
                                          sel(plev=v['level'])
    #
    # Define plot titles.
    plot_names = ['atmosphere: UA - control',
                  'atmosphere: COARE - control',
                  'atmosphere: COARE - UA']
    #
    # Calculate differences.
    data_dict = {}
    data_dict[plot_names[0]] = clim_dict['UA'] - \
                               clim_dict['control']
    data_dict[plot_names[1]] = clim_dict['COARE'] - \
                               clim_dict['control']
    data_dict[plot_names[2]] = clim_dict['COARE'] - \
                               clim_dict['UA']
    #
    # Return data.
    return(data_dict, plot_names)


################################################################################
# Functions to calculate stats.
################################################################################

# Calculate the global mean value of a global map.
# Assumes regular grid and applies cosine weighting.
def global_mean(diff_map):
    #
    # Create map of cosine(latitude) weights.
    cos_map = xr.zeros_like(diff_map)
    cos_map.data = np.broadcast_to(np.expand_dims(
        np.cos(diff_map.lat*np.pi/180.0),axis=1),
                                   diff_map.shape)
    #
    # Calculate mean.
    diff_mean = (diff_map*cos_map).sum()/(cos_map.sum())
    #
    return diff_mean

# Calculate the global RMS value of a global map.
# Assumes regular grid and applies cosine weighting.
def global_RMS(diff_map):
    #
    # Create map of cosine(latitude) weights.
    cos_map = xr.zeros_like(diff_map)
    cos_map.data = np.broadcast_to(np.expand_dims(
        np.cos(diff_map.lat*np.pi/180.0),axis=1),
                                   diff_map.shape)
    #
    # Calculate RMS.
    diff_RMS = np.sqrt((diff_map*diff_map*cos_map).sum()/(cos_map.sum()))
    #
    return diff_RMS

################################################################################
# Functions to plot maps.
################################################################################

def plot_diff_maps(data_dict, name_list, var_name_dict, ssn, interp):
    """Plot precip maps.
    """
    #
    # Get some details based on variable.
    plot_details = plot_info(var_name_dict['atmo'], ssn)
    #
    # Define color map.
    cmap_b = plt.get_cmap('RdBu_r')
    norm_b = colors.BoundaryNorm(np.arange(plot_details['min'],
                                           plot_details['max']*1.01,
                                           plot_details['color_step']),
                                 ncolors=cmap_b.N)
    #
    # Set up axes and figure.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 1),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over axes and plot each.
    for i, ax in enumerate(axgr):
        #
        # Calculate lat and lon meshes
        latm, lonm = np.meshgrid(data_dict[name_list[i]].coords['lat'],
                                 data_dict[name_list[i]].coords['lon'],
                                 indexing='ij')
        #
        # Add other features.
        ax.coastlines()
        ax.set_title(name_list[i],loc='left')
        ax.set_xticks(np.arange(-180,181,60), crs=projection)
        ax.set_yticks(np.arange(-90, 91, 30), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        #
        # Plot the data.
        p = ax.pcolormesh(lonm, latm, data_dict[name_list[i]].data,
                          vmin=plot_details['min'],
                          vmax=plot_details['max'], 
                          transform=projection,
                          cmap=cmap_b, norm=norm_b)
        #
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
        #
        # Add global mean and RMS.
        ax.set_title("{:.2f}".format(global_mean(data_dict[name_list[i]]).data) + ' K',
                     loc='right')
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    #
    # Add color bar.
    cb = axgr.cbar_axes[0].colorbar(p)
    cb.set_label_text(plot_details['units'])
    #
    # Add over all details.
    fig.suptitle(plot_details['main_title'])
    #
    # Get git info for this script.
    sys.path.append('/home/jackre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to footer.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + var_name_dict['atmo'] +
                '_' + ssn +
                '_' + interp +
                '.png',
                format='png')

    return()

def plot_info(v,s):
    """Give some details for plotting, based on variable name.
    Returns a dictionary with units, min and max for color scale.
    """
    # Format season stuff first.
    if s == 'ANN':
        s_string = 'annual mean '
    else:
        s_string = s + ' mean '
    #
    plot_info_dict = {}
    # General details.
    if v == 'T200':                       
        plot_info_dict['min'] = -2.0      
        plot_info_dict['max'] = 2.0       
        plot_info_dict['color_step'] = 0.4
        plot_info_dict['units'] = 'K'
        plot_info_dict['main_title'] = s_string + \
                                       '200 hPa temperature'
    elif v == 'T850':                       
        plot_info_dict['min'] = -2.0      
        plot_info_dict['max'] = 2.0       
        plot_info_dict['color_step'] = 0.4
        plot_info_dict['units'] = 'K'
        plot_info_dict['main_title'] = s_string + \
                                       '850 hPa temperature'
    elif v == 'T200_model':                       
        plot_info_dict['min'] = -2.0      
        plot_info_dict['max'] = 2.0       
        plot_info_dict['color_step'] = 0.4
        plot_info_dict['units'] = 'K'
        plot_info_dict['main_title'] = s_string + \
                                       'model output 200 hPa temperature'
    else:
        sys.exit('Variable name not valid - must be one of (T200,T850,T200_model).')
    return(plot_info_dict)

################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
