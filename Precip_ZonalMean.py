"""
Plot line graphs of zonal mean precipitation.

One line per model run or observational data set.
One panel per season.

Call as:
    $ python Precip_ZonalMean.py interp
with command line argument:
    interp -- the interpolation method: conserve, bilin
All arguments are required and the script fails without them.

"""

###########################################
import sys
import glob
import os
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import cf_units
# For plotting:
import matplotlib
matplotlib.rcParams['hatch.linewidth'] = 0.1
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
#
from AtmoDiffComparison_GlobalMap import seasonal_mean_from_LTA
###########################################

def main():
    #
    # Get command line arguments:
    argv = sys.argv
    if len(argv) != 2:
        sys.exit("Incorrect number of command line arguments (1 required)")
    else:
        interp = check_interp(argv[1])
    #
    # Specify lat and lon range.
    lat_range = np.array([-30.0,30.0])
    lon_range = np.array([60.0,180.0])
    #
    # Load data.
    print('-----\nReading data...\n-----')
    plot_data,plot_names = load_data(interp, lon_range)
    #
    # Plot maps.
    print('-----\nPlotting... \n-----')
    plot_zonal_means(plot_data, plot_names,
                     interp, lat_range, lon_range)
    #
    return()

################################################################################
# Functions to help parse command line arguments.
################################################################################
 
def check_interp(i):
    """Check for valid interpolation method.
    """
    if i in ['conserve', 'bilin']:
        interp = i
    else:
        sys.exit('Interpolation method not valid - must be one of (conserve, bilin).')
    return(interp)

################################################################################
# Functions to load and process data.
################################################################################

def load_data(i,lon_range):
    """Loads data for plot.
    Also defines which runs are included and what their plot titles will be.
    """
    #
    # Define model case names and seasons.
    seasons = ['DJF','MAM','JJA','SON']
    cases = ['20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl',
             '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl',
             '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl']
    plot_names = ['control','UA','COARE']
    #
    # Create data dictionary for all data.
    data_dict = {}
    # Loop over seasons and cases.
    for i_c, c in enumerate(cases):
        # Get name for this case.
        n = plot_names[i_c]
        data_dict[n] = {}
        for i_s, s in enumerate(seasons):
            # Read data.
            fn = glob.glob('/home/jackre/Data/E3SM/' + c +
                           '/atm/climo/180x360/SurfaceMet/' +
                           c + '_' +
                           s + '_*_climo.' + i + '.nc')[0]
            ds = xr.open_dataset(fn, decode_times=False)
            data_dict[n][s] = (ds['PRECC'].isel(time=0).\
                               sel(lon=slice(lon_range[0],lon_range[1]))+
                               ds['PRECL'].isel(time=0).\
                               sel(lon=slice(lon_range[0],lon_range[1]))).\
                               mean(dim='lon')*\
                              (1000.0*60.0*60.0*24.0)
            data_dict[n][s].attrs['units'] = 'mm day-1'
    #
    # Read GPCP data.
    GPCP_filename = '/home/jackre/Data/satellite/GPCP/' + \
                    'GPCP.v2.3.precip.mon.ltm_180x360_GreenwichWest_' + \
                    i + '.nc'
    ds_GPCP = xr.open_dataset(GPCP_filename)
    data_dict['GPCP'] = {}
    plot_names.insert(0,'GPCP')
    for i_s, s in enumerate(seasons):
        s_map = seasonal_mean_from_LTA(ds_GPCP['precip'],s)
        data_dict['GPCP'][s] = s_map.\
                               sel(lon=slice(lon_range[0],lon_range[1])).\
                               mean(dim='lon')
    #
    # Return data.
    return(data_dict, plot_names)

# Calculate the global RMS value of a map.
# Assumes regular grid and applies cosine weighting.
def global_RMS(diff_map):
    #
    # Create map of cosine(latitude) weights.
    cos_map = xr.zeros_like(diff_map)
    cos_map.data = np.broadcast_to(np.expand_dims(
        np.cos(diff_map.lat*np.pi/180.0),axis=1),
                                   diff_map.shape)
    #
    # Calculate RMS.
    diff_RMS = np.sqrt((diff_map*diff_map*cos_map).sum()/(cos_map.sum()))
    #
    return diff_RMS

def coslat_wt_rmse(i,lon_range,lat_range):
    """Cosine-weighted RMS error for zonal mean data.
    
    Calculates RMS error between models in model_name_list, 
    relative to obs_name, with latitude weighting, for each 
    season in the original data. """
    #
    # Load GPCP data.
    GPCP_filename = '/home/jackre/Data/satellite/GPCP/' + \
                    'GPCP.v2.3.precip.mon.ltm_180x360_GreenwichWest_' + \
                    i + '.nc'
    ds_GPCP = xr.open_dataset(GPCP_filename)
    #
    # Define model case names and seasons.
    seasons = ['DJF','MAM','JJA','SON']
    cases = ['20190521.v1_ctl.FC5AV1C-04P2.ne30.cori-knl',
             '20190521.v1_UA.FC5AV1C-04P2.ne30.cori-knl',
             '20190621.v1_COARE.FC5AV1C-04P2.ne30.cori-knl']
    plot_names = ['control','UA','COARE']
    #
    # Create data dictionary for all data.
    RMSE_dict = {}
    # Loop over seasons and cases.
    for i_c, c in enumerate(cases):
        # Get name for this case.
        n = plot_names[i_c]
        RMSE_dict[n] = {}
        for i_s, s in enumerate(seasons):
            # Load model data.
            fn = glob.glob('/home/jackre/Data/E3SM/' + c +
                           '/atm/climo/180x360/SurfaceMet/' +
                           c + '_' +
                           s + '_*_climo.' + i + '.nc')[0]
            ds = xr.open_dataset(fn, decode_times=False)
            # Get GPCP seasonal average.
            s_map = seasonal_mean_from_LTA(ds_GPCP['precip'],s)
            # Calculate difference.
            diff_map = (ds['PRECC'].isel(time=0).\
                        sel(lon=slice(lon_range[0],lon_range[1]),
                            lat=slice(lat_range[0],lat_range[1]))+
                        ds['PRECL'].isel(time=0).\
                        sel(lon=slice(lon_range[0],lon_range[1]),
                            lat=slice(lat_range[0],lat_range[1])))*\
                        (1000.0*60.0*60.0*24.0) - \
                        s_map.sel(lon=slice(lon_range[0],lon_range[1]),
                                  lat=slice(lat_range[0],lat_range[1]))
            RMSE_dict[n][s] = global_RMS(diff_map)
            RMSE_dict[n][s].attrs['units'] = 'mm day-1'
    #
    return RMSE_dict


################################################################################
# Functions to plot maps.
################################################################################

def plot_zonal_means(data_dict, data_names, interp, lat_range, lon_range):
    #
    # Define colors.
    custom_cols = ['#002b36',
                   '#268bd2',
                   '#6c71c4',
                   '#dc322f',
                   '#b58900']
    #
    # Get RMSE values for label printing.
    RMSE = coslat_wt_rmse(interp, lon_range, lat_range)
    #
    # Set up panel and axes.
    fig, axs = plt.subplots(2, 2,
                            figsize=(7, 7),
                            sharey=True, sharex=True)
    #
    # Specify order of panels.
    seasons = ['DJF','JJA','MAM','SON']
    #
    # Loop over seasons then over cases.
    for i_s, s in enumerate(seasons):
        #
        # Set some plot details.
        axs.flatten()[i_s].set_ylim(0.0,12.0)
        axs.flatten()[i_s].set_xlim(lat_range[0],lat_range[1])
        axs.flatten()[i_s].set_title(s,loc='left')
        axs.flatten()[i_s].xaxis.set_tick_params(which='both',
                                                 bottom=True,top=True)
        axs.flatten()[i_s].yaxis.set_tick_params(which='both',
                                                 left=True,right=True)
        #
        # Plot a line.
        for i_c, c in enumerate(data_names):
            axs.flatten()[i_s].plot(data_dict[c][s].lat,
                          data_dict[c][s].data,
                          color=custom_cols[i_c],
                          label=c)
        #
        # Add RMSE strings.
        RMSE_str = 'RMSD (vs. GPCP)\n control= ' + \
              "{:.2f}".format(RMSE['control'][s].data) + \
              '\nUA= ' + "{:.2f}".format(RMSE['UA'][s].data) + \
              '\nCOARE= ' + "{:.2f}".format(RMSE['COARE'][s].data)
        axs.flatten()[i_s].text(lat_range[1]-1, 9.3, RMSE_str,
                                wrap=True,ha='right')
    #
    # Add axis labels.
    for ax in axs[:,0].flatten():
        ax.set_ylabel('precip / ' + r'$mm\ day^{-1}$')
    for ax in axs[-1,:].flatten():
        ax.set_xlabel('latitude / ' + r'$^{\circ}N$')
    #
    # Add legend for just one panel.
    axs.flatten()[1].legend(loc='upper left')
    #
    # Add longitude range for one plot.
    axs[0,0].set_title('longitude ' +
                       "{:.0f}".format(lon_range[0]) + '-' +
                       "{:.0f}".format(lon_range[1]) + r'$^{\circ}E$',
                       loc='right')
    
    #
    # Tidy layout.
    fig.tight_layout()
    #      
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    #
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Show the plot.
    #plt.show()
    # Save the figure.
    plt.savefig('/home/jackre/Documents/plots/OceanFluxes/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_lon_' + "{:.0f}".format(lon_range[0]) +
                '-' + "{:.0f}".format(lon_range[1]) + 
                '_' + interp +
                '.pdf',
                format='pdf')
            
    
################################################################################
# Now actually execute the script.
################################################################################
if __name__ == '__main__':
    main()
        

